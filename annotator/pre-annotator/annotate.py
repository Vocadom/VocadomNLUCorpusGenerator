import re
import glob
import glob

"""
This is a preannotator for corpora in .trs format (SWEET-HOME, VocADom).

The input is a trs file:
<Sync time=564.456>
blabla
<Sync time=564.456>

It can annotate with  intents: set_device, check_device, set_room_property, get_room_property, get_world_property, contact. The relevant slots are then applied.

The output annotation is of Rasa format.


"""
#TO CHANGE:##
path="trs_decembre"
filesToAnnotateNames=glob.glob(path+'/*.trs')
outputFile=open("sweethome_preannotated.json",mode="w",encoding="utf8")
#############

devices=["lumière","télé","ventilateur","volet","store","bouilloire","chauffage","porte","radio","rideau","fenêtre","douche","lampe"]
actions=["diminue","stop","allume","descend","ferme", "baisse","arrête","étei","monte","augmente","ouvr"]
deviceSettings=["allumé","fermé","éteint","ouvert"]
locationRooms=["cuisine","salon","salle de bain","toilette","bureau","corridor","chambre"]
locationFloors=["rez-de-chaussée","étage","d'en haut","du bas","du haut"]
locationInRooms=["chevet", "plafon"]


actionsToSlotValues={"diminue":"TURN_DOWN","stop":"TURN_OFF","allume":"TURN_ON","descend":"LOWER","ferme":"CLOSE", "baisse":"LOWER","arrête":"TURN_OFF","étei":"TURN_OFF","monte":"RAISE","augmente":"TURN_UP","ouvr":"OPEN"}
devicesToSlotValues={"lumière":"light","télé":"tv_music_player","ventilateur":"fan","volet":"shutter","store":"blind","bouilloire":"kettle","chauffage":"heating","porte":"door","radio":"radio","rideau":"curtain","fenêtre":"window","douche":"shower","lampe":"light"}
deviceSettingsToSlotValues={"allumé":"ON","fermé":"CLOSED","éteint":"OFF","ouvert":"OPENED"}
locationRoomsToSlotValues={"cuisine":"kitchen","salon":"living_room","salle de bain":"bathroom","toilette":"wc","bureau":"study","corridor":"couloir","chambre":"bedroom"}
locationFloorsToSlotValues={"rez-de-chaussée":"0","étage":"1","d'en haut":"1","du bas":"0","du haut":"1"}
locationInRoomsToSlotValues={"chevet":"night_table", "plafon":"ceiling"}

recipientToSlotValues={"ma fille":"daughter","ma petite fille":"granddaughter","mon mari":"husband","helena":"relative_name","nom du proche":"relative_name"}


prepLocation=["du ","de la ","de l'","des ","à l'"]
articles=["le ","la ","les ","l'","un","une"]

deviceColor="#00FF00"
actionColor="#FFFF00"
deviceSettingColor="#FFFF00"
locationRoomColor="#FF00FF"
locationFloorColor="#0000FF"
locationInRoomColor="#0FFFFF"



def fillEntityTemplate(start,end,entity,value,text,color):
	'''fills the entity template'''
	template=templateEntity.replace("%%start%%",str(start))
	template=template.replace("%%end%%",str(end))
	template=template.replace("%%entity%%",entity)
	template=template.replace("%%value%%",value)
	template=template.replace("%%text%%",text)
	template=template.replace("%%color%%",color)
	return template

def applyLocationSlot(phrase):
	'''
	if a phrase is a candidate for set_device or set_room_property intent, then we try to find a location slot
	'''
	filledLocationTemplate=""
	filledLocationRoomTemplate=""
	filledLocationFloorTemplate=""
	filledLocationInRoomTemplate=""
	hasLocation=False
	#room
	for locationRoom in locationRooms:
		if locationRoom in phrase:
			locationRoomSlotValue=locationRoomsToSlotValues[locationRoom]
			regex="("+locationRoom+"\w{0,2})\s?"
			m=re.search(regex,phrase)					
			if(m):
				if m.group(1):
					foundLocationRoom=m.group(1)
				
					for preposition in prepLocation:
						if preposition+foundLocationRoom in phrase:
							foundLocationRoom=preposition+foundLocationRoom
							break
					indexLocationRoomBegin=phrase.find(foundLocationRoom)
					indexLocationRoomEnd=indexLocationRoomBegin+len(foundLocationRoom)
					filledLocationRoomTemplate=fillEntityTemplate(indexLocationRoomBegin,indexLocationRoomEnd,"location-room", locationRoomSlotValue,foundLocationRoom,locationRoomColor)
					hasLocation=True
			
	#floor
	for locationFloor in locationFloors:
		
		if locationFloor in phrase:
			locationFloorSlotValue=locationFloorsToSlotValues[locationFloor]
			for preposition in prepLocation:
				if preposition+locationFloor in phrase:
					locationFloor=preposition+locationFloor
					break
			
				
			indexLocationFloorBegin=phrase.find(locationFloor)
			indexLocationFloorEnd=indexLocationFloorBegin+len(locationFloor)
			filledLocationFloorTemplate=fillEntityTemplate(indexLocationFloorBegin,indexLocationFloorEnd,"location-floor", locationFloorSlotValue,locationFloor,locationFloorColor)
			hasLocation=True
	
	#inRoom location (du plafonnier, du chevet etc)
	for locationInRoom in locationInRooms:
		
		if locationInRoom in phrase:
			locationInRoomSlotValue=locationInRoomsToSlotValues[locationInRoom]
			regex="("+locationInRoom+"\w{0,5})\s?"
			m=re.search(regex,phrase)					
			if(m):
				if m.group(1):
					foundLocationInRoom=m.group(1)
					for preposition in prepLocation:
						if preposition+foundLocationInRoom in phrase:
							foundLocationInRoom=preposition+foundLocationInRoom
							break
					indexLocationInRoomBegin=phrase.find(foundLocationInRoom)
					indexLocationInRoomEnd=indexLocationInRoomBegin+len(foundLocationInRoom)
					filledLocationInRoomTemplate=fillEntityTemplate(indexLocationInRoomBegin,indexLocationInRoomEnd,"location-inroom", locationInRoomSlotValue,foundLocationInRoom,locationInRoomColor)
					hasLocation=True

	#concatenating templates
	if filledLocationRoomTemplate!="":
		filledLocationTemplate+=filledLocationRoomTemplate

	if filledLocationFloorTemplate!="":
		if filledLocationTemplate!="":
			filledLocationTemplate+=",\n\t\t\t\t\t"+filledLocationFloorTemplate
		else:
			filledLocationTemplate+=filledLocationFloorTemplate

	if filledLocationInRoomTemplate!="":
		if filledLocationTemplate!="":
			filledLocationTemplate+=",\n\t\t\t\t\t"+filledLocationInRoomTemplate
		else:
			filledLocationTemplate+=filledLocationInRoomTemplate
	
	return hasLocation,filledLocationTemplate

	
def applyCheckDevice(phrase,sync):
	'''
	phrase is annoted as check_device if it has words device and device_setting (in this order; separated by max 10 words)
	'''
	global string
	entitiesToAdd=""
	isAnnotated=False
	
	for device in devices:
		if device in phrase:
			for deviceSetting in deviceSettings:
				if deviceSetting in phrase:
					deviceSettingSlotValue=deviceSettingsToSlotValues[deviceSetting]
					deviceSlotValue=devicesToSlotValues[device]				
				
					#regexS="("+action+".*?)\s(?:\w*\s)?("+device+".*)(?:\s)?"
					#regexS="("+action+".*?)\s(?:\w*\s)?("+device+"(?:\w)?)(?:\s)?"
					regex="("+device+"\w{0,3})\s(?:[\w'-]*\s){0,9}("+deviceSetting+"(?:\w{0,3})?)(?:\s)?"
					

					m=re.search(regex,phrase)					
					
					if(m):
						if m.group(1):
							foundDevice=m.group(1)
							#looking for the article before the device, to include it in the slot
							for article in articles:
								if article+foundDevice in phrase:
									foundDevice=article+foundDevice
									break
							indexDeviceBegin=phrase.find(foundDevice)
							indexDeviceEnd=indexDeviceBegin+len(foundDevice)
							
							if m.group(2):
								
								foundDeviceSetting=m.group(2)
								indexDeviceSettingBegin=phrase.find(foundDeviceSetting)
								indexDeviceSettingEnd=indexDeviceSettingBegin+len(foundDeviceSetting)
								
								
								#found all necessary values, time to insert them in the template
								#first fill the template for each entity
								
								#devicesetting
								filledDeviceSettingTemplate=fillEntityTemplate(indexDeviceSettingBegin,indexDeviceSettingEnd,"device-setting",deviceSettingSlotValue,foundDeviceSetting,deviceSettingColor)
								#device
								filledDeviceTemplate=fillEntityTemplate(indexDeviceBegin,indexDeviceEnd,"device",deviceSlotValue,foundDevice,deviceColor)
								if entitiesToAdd!="": #if it is not the first couple action+device in this phrase
									entitiesToAdd+=",\n\t\t\t\t\t"
								entitiesToAdd+=filledDeviceTemplate+",\n\t\t\t\t\t"+filledDeviceSettingTemplate
								
								isAnnotated=True
								
	if isAnnotated:
		#now check if there is location slot
		hasLocation,filledLocationTemplate=applyLocationSlot(phrase)
		if hasLocation:
			entitiesToAdd+=",\n\t\t\t\t\t"+filledLocationTemplate
									
		#now fill the template for the whole phrase
		template=templateWithEntities.replace("%%text%%",phrase)
		template=template.replace("%%sync%%",sync)
		template=template.replace("%%TO_SUBSTITUTE_BY_TEMPLATE_ENTITY%%",entitiesToAdd)
		
		#checking if check_device or check_device_group:
		if foundDevice[-1]=="s" or foundDevice[-3:]=="aux":
			template=template.replace("%%intent%%","check_device_group")
		else:		
			template=template.replace("%%intent%%","check_device")
									
		string+=template+"\n"
	return isAnnotated
					
	
def applySetDevice(phrase,sync):
	'''
	phrase is annoted as set_device if it has words action and device (in this order; separated by max 2 words)
	'''
	global string
	entitiesToAdd=""
	isAnnotated=False
	
	for device in devices:
		if device in phrase:
			for action in actions:
				if action in phrase:
					actionSlotValue=actionsToSlotValues[action]
					deviceSlotValue=devicesToSlotValues[device]				
					
					#regexS="("+action+".*?)\s(?:\w*\s)?("+device+".*)(?:\s)?"
					#regexS="("+action+".*?)\s(?:\w*\s)?("+device+"(?:\w)?)(?:\s)?"
					#regexS="("+action+"\w{0,3})\s(?:\w*\s){0,2}("+device+"(?:\w{0,3})?)(?:\s)?"
					regex="("+action+"\w{0,4})\s(?:[\w']*\s){0,2}("+device+"(?:\w{0,3})?)(?:\s)?" #added apostrophe for words with l'

					m=re.search(regex,phrase)					
					
					if(m):
						if m.group(1):
							foundAction=m.group(1)
							indexActionBegin=phrase.find(foundAction)
							indexActionEnd=indexActionBegin+len(foundAction)
							
							if m.group(2):
								foundDevice=m.group(2)
								#looking for the article before the device, to include it in the slot
								for article in articles:
									if article+foundDevice in phrase:
										foundDevice=article+foundDevice
										break
								indexDeviceBegin=phrase.find(foundDevice)
								indexDeviceEnd=indexDeviceBegin+len(foundDevice)
								
								
								#found all necessary values, time to insert them in the template
								#first fill the template for each entity
								
								#action
								filledActionTemplate=fillEntityTemplate(indexActionBegin,indexActionEnd,"action",actionSlotValue,foundAction,actionColor)
								#device
								filledDeviceTemplate=fillEntityTemplate(indexDeviceBegin,indexDeviceEnd,"device",deviceSlotValue,foundDevice,deviceColor)
								
								if entitiesToAdd!="": #if it is not the first couple action+device in this phrase
									entitiesToAdd+=",\n\t\t\t\t\t"
								entitiesToAdd+=filledActionTemplate+",\n\t\t\t\t\t"+filledDeviceTemplate
								
								isAnnotated=True
	if isAnnotated:
		#now check if there is location slot
		hasLocation,filledLocationTemplate=applyLocationSlot(phrase)
		if hasLocation:
			entitiesToAdd+=",\n\t\t\t\t\t"+filledLocationTemplate
									
		#now fill the template for the whole phrase
		template=templateWithEntities.replace("%%text%%",phrase)
		template=template.replace("%%sync%%",sync)
		template=template.replace("%%TO_SUBSTITUTE_BY_TEMPLATE_ENTITY%%",entitiesToAdd)
		
		#checking if set_device or set_device_group:
		if foundDevice[-1]=="s" or foundDevice[-3:]=="aux":
			template=template.replace("%%intent%%","set_device_group")
		else:		
			template=template.replace("%%intent%%","set_device")
									
		string+=template+"\n"
	

	return isAnnotated
			



templateWithEntities="""			{
				"text": "%%text%%",
				"sync_trs": "%%sync%%",
				"entities": [
					%%TO_SUBSTITUTE_BY_TEMPLATE_ENTITY%%
				],
				"intent": "%%intent%%"
			},"""

templateEntity="""{
						"start": %%start%%,
						"end": %%end%%,
						"entity": "%%entity%%",
						"value": "%%value%%",
						"text": "%%text%%",
						"color": "%%color%%"
					}"""


templateNoEntities="""			{
				"text": "%%text%%",
				"sync_trs": "%%sync%%",
				"entities": []
			},"""


def applySetRoomProperty(phrase,sync):
	'''
	'''
	global string
	#set_room_property
	if "température" in phrase:
		for action in actions:
			if action in phrase:
				actionSlotValue=actionsToSlotValues[action]
				regexAction="("+action+".*?)\s?"
				m=re.search(regexAction,phrase)
				if(m):
					if m.group(1):
						foundAction=m.group(1)
						indexActionBegin=phrase.find(foundAction)
						indexActionEnd=indexActionBegin+len(foundAction)
						foundTemperature="température"
						for article in articles:
							if article+"température" in phrase:
								foundTemperature=article+"température"
						indexTemperatureBegin=phrase.find(foundTemperature)
						indexTemperatureEnd=indexTemperatureBegin+len(foundTemperature)
						filledActionTemplate=fillEntityTemplate(indexActionBegin,indexActionEnd,"action",actionSlotValue,foundAction,actionColor)
						filledTemperatureTemplate=fillEntityTemplate(indexTemperatureBegin,indexTemperatureEnd,"room-property","temperature",foundTemperature,deviceColor)
						entitiesToAdd=filledActionTemplate+",\n\t\t\t\t\t"+filledTemperatureTemplate

						#now fill the template for the whole phrase
						template=templateWithEntities.replace("%%text%%",phrase)
						template=template.replace("%%sync%%",sync)
						template=template.replace("%%TO_SUBSTITUTE_BY_TEMPLATE_ENTITY%%",entitiesToAdd)
						template=template.replace("%%intent%%","set_room_property")
								
						string+=template+"\n"
							
						return True
	return False

def applyGetRoomProperty(phrase,sync):
	'''
	'''
	global string
	#get_room_property
	if "température" in phrase:
		if "donne" in phrase:
				regexAction="(donne.*?)\s?"
				m=re.search(regexAction,phrase)
				if(m):
					if m.group(1):
						foundTemperature="température"
						for article in articles:
							if article+"température" in phrase:
								foundTemperature=article+"température"
						indexTemperatureBegin=phrase.find(foundTemperature)
						indexTemperatureEnd=indexTemperatureBegin+len(foundTemperature)

						filledTemperatureTemplate=fillEntityTemplate(indexTemperatureBegin,indexTemperatureEnd,"room-property","temperature",foundTemperature,deviceColor)
						entitiesToAdd=filledTemperatureTemplate

						#now fill the template for the whole phrase
						template=templateWithEntities.replace("%%text%%",phrase)
						template=template.replace("%%sync%%",sync)
						template=template.replace("%%TO_SUBSTITUTE_BY_TEMPLATE_ENTITY%%",entitiesToAdd)
						template=template.replace("%%intent%%","get_room_property")
								
						string+=template+"\n"
							
						return True
	return False

def applyGetWorldProperty(phrase,sync):
	'''
	'''
	global string
	#get_world_property
	if "l\'heure" in phrase:
		if "donne" in phrase:
			indexTimeBegin=phrase.find("l\'heure")
			indexTimeEnd=indexTimeBegin+len("l\'heure")

			filledTimeTemplate=fillEntityTemplate(indexTimeBegin,indexTimeEnd,"world-property","time","l\'heure",deviceColor)
			entitiesToAdd=filledTimeTemplate

			#now fill the template for the whole phrase
			template=templateWithEntities.replace("%%text%%",phrase)
			template=template.replace("%%sync%%",sync)
			template=template.replace("%%TO_SUBSTITUTE_BY_TEMPLATE_ENTITY%%",entitiesToAdd)
			template=template.replace("%%intent%%","get_world_property")
								
			string+=template+"\n"
							
			return True
	return False


def applyContact(phrase,sync):
	'''
	'''
	global string
	#contact
	if "appelle" in phrase:
		for recipient,recipientVal in recipientToSlotValues.items():
			if recipient in phrase:
				indexContactBegin=phrase.find("appelle")
				indexContactEnd=indexContactBegin+len("appelle")
				indexRecipientBegin=phrase.find(recipient)
				indexRecipientEnd=indexRecipientBegin+len(recipient)

				filledActionTemplate=fillEntityTemplate(indexContactBegin,indexContactEnd,"action","contact","appelle",actionColor)
				filledRecipientTemplate=fillEntityTemplate(indexRecipientBegin,indexRecipientEnd,"recipient",recipientVal,recipient,deviceColor)
				entitiesToAdd=filledActionTemplate+",\n\t\t\t\t\t"+filledRecipientTemplate

				#now fill the template for the whole phrase
				template=templateWithEntities.replace("%%text%%",phrase)
				template=template.replace("%%sync%%",sync)
				template=template.replace("%%TO_SUBSTITUTE_BY_TEMPLATE_ENTITY%%",entitiesToAdd)
				template=template.replace("%%intent%%","contact")				
				string+=template+"\n"
				return True
	return False



string=""
for fileToAnnotate in filesToAnnotateNames:
	f=open(fileToAnnotate, mode="r",encoding="iso-8859-1")
	content=f.read()
	content=content.encode().decode('utf8')

	#the string we will write in the output file in the end
	

	#regex for capturing one production (including empty production) at a time
	regex = re.compile("<Sync time=\"(.+?)\"\/>\n(.+?)\n<Sync time=\".+?\/>")
	productions = re.findall(regex, content)

	for i in range(0,len(productions)):
		if productions[i][1]!="None":
			sync=productions[i][0]
			phrase=productions[i][1]
			phrase=phrase.strip()
		
			#trying to annotate with set_device
			phraseIsAnnotated=applySetDevice(phrase,sync)
		
			if phraseIsAnnotated==True:
				continue
		
			#trying to annotate with applyCheckDevice
			phraseIsAnnotated=applyCheckDevice(phrase,sync)
		
			if phraseIsAnnotated==True:
				continue
			#trying to annotate with set_room_property
			phraseIsAnnotated=applySetRoomProperty(phrase,sync)
		
			if phraseIsAnnotated==True:
				continue
		
			#trying to annotate with get_room_property
			phraseIsAnnotated=applyGetRoomProperty(phrase,sync)
		
			if phraseIsAnnotated==True:
				continue

			#trying to annotate with get_world_property
			phraseIsAnnotated=applyGetWorldProperty(phrase,sync)
		
			if phraseIsAnnotated==True:
				continue
			
			#trying to annotate with contact
			phraseIsAnnotated=applyContact(phrase,sync)
		
			if phraseIsAnnotated==True:
				continue
			
			#processing of phrases without entities
			textToAdd=templateNoEntities.replace("%%text%%",phrase)
			textToAdd=textToAdd.replace("%%sync%%",sync)
			string+=textToAdd+"\n"
	


#need to strip the last comma
string=string.strip(',\n')

bigTemplateFile=open("template.json",mode="r",encoding="iso-8859-1")
bigTemplate=bigTemplateFile.read()
bigTemplate=bigTemplate.encode().decode('utf8')
bigTemplate=bigTemplate.replace("%%TO_SUBSTITUTE%%",string)
	

outputFile.write(bigTemplate)
f.close()
outputFile.close()
