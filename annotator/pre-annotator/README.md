This is a pre-annotator tool for corpora in .trs format (VocADom, SWEET-HOME).

Please change the variables: path and filesToAnnotateNames inside the script.
The output is a pre-annotated json file ready to be manually checked in the interface ./../tool_web_annotator. Please refer to the README file of tool_web_annotator to know how to do it. 

NB: the resulting pre-annotation takes the "allowable_values" from template.json, which are fixed. However it doesn't mean that you cannot use your own values during the annotation.


