//utility functions

function sort_examples_by_start(a,b) {
	// a and b are objects/dictionaries with numeric property start
	if(a.start < b.start) {
		return -1;
	}
	if(a.start > b.start) {
		return 1;
	}
	if(a.start == b.start) {
		return 0;
	}
}

function copy(d) {
	//copy a list
	return d.slice();
}

function copy_dict(d) {
	//copy a dictionary
	return $.extend({}, d);
}

/*
function create_inner_highlighted_text(highlights,text){
	s = "";

	last_written_i = 0;
	if(highlights.length == 0) {
		s += text;
	}
	else {
		for(i = 0; i < highlights.length; i++) {
			item = highlights[i];
			if(last_written_i < item.start){
				s += text.slice(last_written_i,item.start)
			}
			item.text = text.slice(item.start,item.end);
			//item.slice = ;
			s += $("#highlightsliceTemplate").render(item);
			//s += '<span style="background-color: ' + item.color + '">' + text.slice(item.start,item.end) + '</span>'
			last_written_i = item.end
		}
		if(last_written_i < text.length) {
			s += text.slice(last_written_i,text.length);
		}
	}	
	
	return s;
	
}*/

function get_slices(highlights,text) {
	var slices = [];
	
	last_written_i = 0;
	if(highlights.length == 0) {
		slices.push({text: text, color: null});
	}
	else {
		for(i = 0; i < highlights.length; i++) {
			item = highlights[i];
			if(last_written_i < item.start){
				slices.push({text: text.slice(last_written_i,item.start), color: null});
				//s += text.slice(last_written_i,item.start)
			}
			//item.text = text.slice(item.start,item.end);
			//item.slice = ;
			slices.push({text: text.slice(item.start,item.end), color: item.color});
			//s += $("#highlightsliceTemplate").render(item);
			//s += '<span style="background-color: ' + item.color + '">' + text.slice(item.start,item.end) + '</span>'
			last_written_i = item.end;
		}
		if(last_written_i < text.length) {
			slices.push({text: text.slice(last_written_i,text.length), color: null});
			//s += text.slice(last_written_i,text.length);
		}
	}	
	
	return slices;	
}

function numeric_extender(target, precision) {
			//create a writable computed observable to intercept writes to our observable
			var result = ko.pureComputed({
				read: target,  //always return the original observables value
				write: function(newValue) {
					var current = target(),
						roundingMultiplier = Math.pow(10, precision),
						newValueAsNum = isNaN(newValue) ? 0 : +newValue,
						valueToWrite = Math.round(newValueAsNum * roundingMultiplier) / roundingMultiplier;
		 
					//only write if it changed
					if (valueToWrite !== current) {
						target(valueToWrite);
					} else {
						//if the rounded value is the same, but a different value was written, force a notification for the current field
						if (newValue !== current) {
							target.notifySubscribers(valueToWrite);
						}
					}
				}
			}).extend({ notify: 'always' });
		 
			//initialize with current value to make sure it is rounded appropriately
			result(parseInt(target()));
		 
			//return the new computed observable
			return result;
	}		

function associate_colors_with_entities_obs(entities) {
	
	for (i = 0; i < entities.length; i++) {
		entities[i]().color(get_color(i));
	}
	
	return entities;
}

function associate_colors_with_entities(entities) {
	
	for (i = 0; i < entities.length; i++) {
		entities[i].color = get_color(i); 
	}
	
	return entities;
}


function get_color(i) {
	//Get a highlight color from the set, given an "index" 
	colors = ["#FFFF00","#00FF00","#FF00FF","#00FFFF"]

	if(i < colors.length) {
		return color = colors[i]; 
	}
	else {
		return colors[i % colors.length];
	}
	
}

function get_new_color_for_entity(len_existing) {
	return get_color(len_existing+1);	
}

function get_highlights(sorted_entities) {
	//entities must have the following keys: 
	// start, end, color
	highlights = []
	for (i = 0; i < sorted_entities.length; i++) {
		item = JSON.parse(JSON.stringify(sorted_entities[i])); 
		
		if(item.start == -1 || item.end == -1) {
			continue;
		}
		
		
		overlapped_with_one = false;
		chunk_remains = false;
		chunk_remnant = -1;
		//check all existing highlights...
		j = 0;
		while(j < highlights.length) {
			
			//if it's before the whole thing...
			if(item.start < highlights[j].start && item.end < highlights[j].end) {
				j++;
				continue;
			}
				
			
			//item starts before the last one ended
			if(item.start < highlights[j].end) {
				overlapped_with_one = true;
				
				//item is actually completely engulfed by the existing highlight
				if(item.end < highlights[j].end) {
					//add the overlap
					highlights.splice(j+1,0,{
						start: item.start,
						end: item.end,
						color: $.xcolor.average(highlights[j].color, item.color).getHex()
						});
					//add the end bit from the old highlight
					highlights.splice(j+2,0,{
						start: item.end, 
						end: highlights[j].end, 
						color: highlights[j].color}); 
					//update the first bit to reflect the new ending
					highlights[j].end = item.start;
					j+=2
					chunk_remains = false;
				} 
				
				if(item.end == highlights[j].end) {
					//add the overlap
					highlights.splice(j+1,0,{
						start: item.start,
						end: item.end,
						color: $.xcolor.average(highlights[j].color, item.color).getHex()
						});

					//update the first bit to reflect the new ending
					highlights[j].end = item.start;
					j+=1;
					chunk_remains = false;
				} 				
				
				//item extends past the end of the existing highlight!
				if(item.end > highlights[j].end) {
					
					//add the overlap
					highlights.splice(j+1,0,{
						start: item.start,
						end: highlights[j].end,
						color: $.xcolor.average(highlights[j].color, item.color).getHex()
						});

					//update the first bit to reflect the new ending
					old_highlight_end = highlights[j].end;
					highlights[j].end = item.start;
					
					//now we have a chunk left that we haven't highlighted yet
					chunk_remains = true
					chunk_remnant = i
					//update item's start to be the new chunk that we haven't dealt with
					item.start = old_highlight_end;
					//overlapped_with_one = false;
					

					j+=1;
				}

			}
			j++;
		}
		
		if(!overlapped_with_one){
			//simply add the new highlight, since it doesn't interfere with anything
			highlights.push({
				start: item.start,
				end: item.end,
				color: item.color
			})
		}
		
		if(chunk_remains){
		//simply add the new highlight, since it doesn't interfere with anything
			highlights.push({
				start: item.start,
				end: item.end,
				color: item.color
			})
		}
	}
	
	//console.log(highlights);
	return highlights;
		
}