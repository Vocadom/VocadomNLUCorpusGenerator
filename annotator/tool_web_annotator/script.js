$(document).ready(function() {
	//Setup the document. Attach the two viewmodels to the two separate divs in the document.
	
	var dataviewModel = new DataViewModel();
	var topbarviewModel = new TopbarViewModel(dataviewModel.get_data,dataviewModel);
	
	topbarviewModel.events.subscribe(dataviewModel.on_new_data_ready, dataviewModel, "new_data_ready");
	//dataivewModel.events.subscrive(topbarviewModel., this, "new_data_ready_to_save");
	
	ko.applyBindings(topbarviewModel, $('#topbar_menu').get(0));
	ko.applyBindings(dataviewModel, $('#data_column').get(0));

});



