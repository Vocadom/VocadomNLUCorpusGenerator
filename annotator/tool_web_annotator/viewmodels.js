//ViewModels - where the ui is connected to the datastructures.

class TopbarViewModel {

	constructor(get_data_func, data_func_executor) {
		this.events = new ko.subscribable();
		this.file_name = ko.observable("");
		this.get_data_func = function() {
			return get_data_func(data_func_executor);
		}
		//this.download_file_name = ko.computed(this.get_downloaded_file_name,this);
		this.download_available = ko.observable(false);
		this.download_data = ko.observable("");
	}

	load_file(self, evt) {
		var file = evt.target.files[0];
		self.file_name(file.name);
		//self.file = file;
		
		var reader = new FileReader();
		reader.readAsText(file);
        reader.onload = function(e) {
			var data = JSON.parse(e.target.result);
			self.events.notifySubscribers(data, "new_data_ready");
			//this.data = JSON.parse(e.target.result);
			//.set_data(JSON.parse(e.target.result));
			//example_manager.reset_list(); //reset_page(example_manager.data);
        }	
	}
	
	save_file(self, example_manager,evt) {
		self.download_available(true);

		var retdata = self.get_data_func();
		var data = {'allowable_values': {
					'entity':retdata[1][1],
					'intent':retdata[1][0],
					'value':retdata[1][2]
					},
					'rasa_nlu_data': {'common_examples': retdata[0]}}; //{'one': 1, 'two': 2}; //TODO
		this.download_data("data:text/plain;charset=UTF-8,"  + encodeURIComponent(JSON.stringify(data,null,4)));
	}
	
	download_file(self,evt) {
		this.download_available(false);
		return true;
	}
	
}

class DataViewModel {
	constructor() {
		
		this.events = new ko.subscribable();
		this.self = this;
		
		this.data = ko.observableArray();
		
		this.da_list = ko.observableArray([]);
		this.slot_list = ko.observableArray([]);
		this.slot_value_list = ko.observableArray([]);
		
		//---current item-------------------------------
		this.current_item_data = ko.observable({
			text:ko.observable(null),
			intent:ko.observable(null)});
		//	entities: ko.observableArray([])});
		this.current_item_data_entities = ko.observableArray();
		this.current_item_elem = ko.observable(null);
		this.current_item_original_data = ko.observable(null);
		this.actual_current_item_original_data = null;
		this.current_item_calculated_text = ko.computed(function() {
			$.each(this.current_item_data_entities(), function(id,entity) {
				entity().start();
				entity().end();
			});
			this.current_item_data()
			return this.get_current_highlighted_slices();
		},this.self);
	
			
		this.test_func = function(item) { 
		if(item == this.current_item_original_data()){
			return 'detailTemplate';
		} else {
			return 'nullTemplate';
		}
		}.bind(this);	

		//new entity------------------------------------
		this.new_entity = ko.observable({
			'start': ko.observable(-1).extend({ numeric: 0 }),
			'end': ko.observable(-1).extend({ numeric: 0 }),
			'color': null,
			'text': null,
			'entity': ko.observable(null),
			'value': ko.observable(null)
		});
		this.new_entity().color = ko.computed(function() {
			return get_new_color_for_entity(this.current_item_data_entities().length);
		},this);
		this.new_entity().text = ko.computed(function() {
					if(this.current_item_data().text() == null) {
						return '';
					} else {
						return this.current_item_data().text().slice(this.new_entity().start(),this.new_entity().end());
					}
			},this);	
		
		ko.extenders.numeric = numeric_extender;
	}

	
	//---ADD AND REMOVE EXAMPLES----------------------------------
	add_and_edit_new_example() {
		var new_example = {text: "", intent: "", entities:[], highlighted_text: ko.observable("")};
		this.data.splice(0,0,new_example);
		//this.display_example(this,new_example,null);
	}
	
	delete_example(self,item,evt) {
		self.data.remove(item);
	}
		
	
	//---SAVING-------------------------------------------------
	
	get_data(self) {
		return [self.data(),[self.da_list(),self.slot_list(),self.slot_value_list()]];
	}
	
	//---OTHER--------------------------------------------------
	
	flatten_current_entities(){
		var new_entities = [];
		$.each(this.current_item_data_entities(), function(id,entity) {
			var new_entity = {};
			for(var key in entity()) {
				new_entity[key] = entity()[key]()
			}
			new_entities.push(new_entity);
		});
		return new_entities;
	}
	
	copy_entity_observable(self,entity_observable) {
		var new_entity = ko.observable({});
		new_entity().start = ko.observable(entity_observable().start()).extend({ numeric: 0 });
		new_entity().end = ko.observable(entity_observable().end()).extend({ numeric: 0 });
		new_entity().entity = ko.observable(entity_observable().entity());
		new_entity().value = ko.observable(entity_observable().value());
		new_entity().text = ko.computed(function() {
			return self.current_item_data().text().slice(new_entity().start(),new_entity().end());
		},new_entity);
		new_entity().color = ko.observable(entity_observable().color());	
		return new_entity;
	}


	add_new_entity(self,item,evt) {
		console.log('in new entity');
		console.log(item);
		console.log(evt);
		
		var selection_start = document.getElementById('new_text').selectionStart;
		var selection_end = document.getElementById('new_text').selectionEnd;
		//console.log(selected_data);
		//if(selected_data.text != "" && selected_data.node.nodeName == "#new_text") {
		if(selection_start != -1 && selection_end != -1) {
			//var start = $("#text").text.search(selected_data.text);
			//var end = null;
			//if(start == -1) {
			//	end == -1;
			//} else {
			//	end = start + selected_data.text.length;
			//}
			if(selection_start != selection_end) {
				self.new_entity().start(selection_start);
				self.new_entity().end(selection_end);
			}

		}
		//self.current_item_data_entities.push(self.copy_entity_observable(self,self.new_entity));
		//self.current_item_data_entities.splice(2,0,someNewItem);
		self.add_entity_to_entities(self.copy_entity_observable(self,self.new_entity));
		//associate_colors_with_entities_obs(self.current_item_data_entities());

		self.new_entity().start(-1);
		self.new_entity().end(-1);
		self.new_entity().entity(null);
		self.new_entity().value(null);
		//self.new_entity().color(null);		
	}
	
	update_entity(self,item,evt) {
		console.log('hold');
		
		self.actual_current_item_original_data.intent = self.current_item_data().intent();
		self.actual_current_item_original_data.text = self.current_item_data().text();
		//self.actual_current_item_original_data = copy_dict(self.current_item_data());
		
		var new_entities = self.flatten_current_entities();
		self.actual_current_item_original_data.entities = new_entities;
		self.actual_current_item_original_data.highlighted_text(self.get_highlighted_slices(self.actual_current_item_original_data));
		//this.current_item_original_data(new_data);
		
		//now update the allowable lists
		self.add_to_dact_list(self.current_item_data().intent);
		for(var i in new_entities) {
			var entity = new_entities[i];
			self.add_to_entity_list(entity.entity);
			self.add_to_value_list(entity.value);
		}
	}
	
	add_to_dact_list(intent){
		if(this.da_list().indexOf(intent) == -1){
			this.da_list.push(intent);
		}		
	}
	
	add_to_entity_list(slot_label){
		if(this.slot_list().indexOf(slot_label) == -1){
			this.slot_list.push(slot_label);
		}
	}
	
	add_to_value_list(slot_value) {
		if(this.slot_value_list().indexOf(slot_value) == -1) {
			this.slot_value_list.push(slot_value);
		}		
	}
	
	add_entity_to_entities(new_entity) {
		var i = 0;
		for (var j = 0 ; j < this.current_item_data_entities().length; j++) {
			if(this.current_item_data_entities()[j]().start() < new_entity().start()) {
				i = j;
			} else {
				i = j; 
				break;
			}
		}
		this.current_item_data_entities.splice(i,0,new_entity);
		
	}
	
	get_current_text_slice(entity) {
		return this.current_item_data().text().slice(entity.start,entity.end);
	}
	
	on_new_data_ready(newValue) {
		console.log('got new data');
		
		//add the allowable slots...
		var allowable_values = newValue.allowable_values
		for(var i in allowable_values.intent){
			this.add_to_dact_list(allowable_values.intent[i]);
		}
		for(var i in allowable_values.entity){
			this.add_to_entity_list(allowable_values.entity[i]);
		}
		for(var i in allowable_values.value){
			this.add_to_value_list(allowable_values.value[i]);
		}	

		//now add from the actual data			
		var data = newValue.rasa_nlu_data.common_examples
		for(var i in data) {
			var example = data[i];
			example.highlighted_text = ko.observable(this.get_highlighted_slices(example));
		}
		for(var i in data) {
			var example = data[i];
			this.add_to_dact_list(example.intent);
			
			for (var j in example.entities) {
				var entity = example.entities[j];
				this.add_to_entity_list(entity.entity);
				this.add_to_value_list(entity.value);
			}
		}
		this.data(data);
	}
	
	get_highlighted_slices(data) {
		var text = data.text;
		
		var sorted_entities = copy(data.entities).sort(sort_examples_by_start);
		associate_colors_with_entities(sorted_entities);
		highlights = get_highlights(sorted_entities);

		return get_slices(highlights,text);
	}
	
	delete_entity(self,data,evt) {
		for(var i = 0; i < self.current_item_data_entities().length; i++) {
			if(self.current_item_data_entities()[i]() == data) {
				self.current_item_data_entities.splice(i,1);
				break;
			}
		}
	}
	
	get_current_highlighted_slices() {
		var text = this.current_item_data().text();
		if(text == null){
			return [];
		}
		
		var flat_entities = this.flatten_current_entities();
		var sorted_entities = flat_entities.sort(sort_examples_by_start);
		//associate_colors_with_entities(sorted_entities_flat);
		highlights = get_highlights(sorted_entities);

		return get_slices(highlights,text);
	}	
	
	display_example(self,data,evt) {
		if(evt.currentTarget != self.current_item_elem()) {
			//data - is the original data item in the observableArray - we need to not edit this, so make a copy
			
			//safe string copies
			self.current_item_data().text(data.text); 
			self.current_item_data().intent(data.intent);
			
			//sort the entities and associate them with colors
			//so: self.curent_item_data_entities are sorted and colored already!!!
			var sorted_entities = copy(data.entities).sort(sort_examples_by_start);
			associate_colors_with_entities(sorted_entities);
			
			//fill the entities
			self.current_item_data_entities.removeAll();
			
			$.each(sorted_entities, function(id,entity){
				
				var ent = self.create_entity_from_dict(self,entity)
				
				self.current_item_data_entities.push(ent);
				ent.subscribe(self.current_item_calculated_text);
			});
			
			self.current_item_original_data(data);
			self.actual_current_item_original_data = data;
			self.current_item_elem(evt.currentTarget);
		}
	}
	
	create_entity_from_dict(self,entity) {
		//add each entity...
		var new_ent = ko.observable({});
		new_ent().start = ko.observable(entity.start).extend({ numeric: 0 });
		new_ent().end = ko.observable(entity.end).extend({ numeric: 0 });
		new_ent().value = ko.observable(entity.value);
		new_ent().entity = ko.observable(entity.entity);
		new_ent().color = ko.observable(entity.color);
		
		new_ent().text = ko.computed(function() {
			return self.current_item_data().text().slice(new_ent().start(),new_ent().end());
		},new_ent);			
		return new_ent;
	}
	

	
}
