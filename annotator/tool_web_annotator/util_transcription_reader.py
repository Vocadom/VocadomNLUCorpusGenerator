# Basic transcription xml utility
import xml.etree.ElementTree as ET
import codecs
#FORMAT:
# trans
#	episode

def get_xml_data_from_file(filename):
	#with codecs.open(filename, 'r') as xml_file:
	#	xml_tree = ET.parse(xml_file)
	#return xml_tree
	return ET.parse(filename)

def get_trans_node(xml_data):
	return xml_data.getroot()
	
def get_turn_nodes(xml_data):
	trans = get_trans_node(xml_data)
	episode = trans.findall('Episode')
	assert(len(episode) == 1)
	section = episode[0].findall('Section')
	assert(len(section) == 1)
	turns = section[0].findall('Turn')
	return turns