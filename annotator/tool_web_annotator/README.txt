Annotator tool

Refer to Technical Report: Web browser-based corpora annotator tool 
in hephaistos/archives/Old_members/archiv_stagiaires/Stefania.Raimondo2017/GrenobleSmartHome/report_technical/technical_report.pdf

This tool is an interface for manual annotation of corpora.

2 ways to work with this tool:

1. if you want to annotate the .trs file from scratch (not pre-annotated), you need:
Run python3 basic_transription_reader.py input_filename <not_annotated.trs> annotation_filename annotation_allowable_list.json

The output file will be a json in RASA format, an empty template with phrases, ready to be annotated manually in the interface. Click index.html and upload that output file. The process of annotation implies filling the required fields for each phrase. Dowload the resulting annotated json.

2. If you used the pre-annotation tool (./../pre-annotator/annotate.py) which takes a trs. file and pre-annotates it, you are supposed to have a json file containing the annotation ready to be checked in the interface. Click index.html and upload that file. The process of annotation implies filling the required fields for each phrase. Dowload the resulting annotated json.

