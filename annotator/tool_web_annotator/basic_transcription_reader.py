# Basic transcription reader
# Used for reading xml transcription (ie trs) files
# Format:
#  1. There is only one turn. 
#  2. All utterances occur on a single line
#  3. A turn contains only one level deep of tags (ie Sync/Event closed tags, but no sub tags)
#  4. All utterances are tails of these one level deep tags
#  5. 'None' marks a null utterance
#

import codecs
import xml.etree.ElementTree as ET
import util_transcription_reader as utr
import os
import json

################################
def get_single_turn_node(xml_data):
	turn_nodes = utr.get_turn_nodes(xml_data)
	assert(len(turn_nodes) == 1)
	return turn_nodes[0]
	
def get_all_tail_text(turn_node):
	sync_times = []
	curr_sync = None
	utterances = []
	for child in turn_node:
		if(child.tag == "Sync"):
			curr_sync = child
			
		tail = child.tail
		if(tail != None):
			tail = tail.strip()
			if(tail != 'None' and tail != ""):
				#assert(child.tag == "Sync"), child.tag #should always be after a sync tag
				sync_times.append(curr_sync.attrib['time'])
				utterances.append(tail)
	return utterances, sync_times
	
##################################
def get_all_utterances(filename):
	xml_data = utr.get_xml_data_from_file(filename)
	turn_node = get_single_turn_node(xml_data)
	utterances, sync_times = get_all_tail_text(turn_node)
	return utterances, sync_times
	
def write_to_annotator_tool_format(utterances, sync_times, allowable_list_of_slots_dict, output_file, default_intent=None):
	output_data = {'allowable_values':{'entity':[], 'intent':[], 'value':[]},
					"rasa_nlu_data": {'common_examples':[]}}
					
	#set the allowable values
	for key in allowable_list_of_slots_dict:
		assert(key in output_data['allowable_values'])
		output_data['allowable_values'][key] = allowable_list_of_slots_dict[key]
					
	#set the actual text
	for i in range(len(utterances)):
		utterance = utterances[i]
		sync_time = sync_times[i]
		if(default_intent != None):
			ex = {'text':utterance, 'intent': default_intent, 'entities':[], 'sync_trs':sync_time}
		else:
			ex = {'text':utterance, 'entities':[], 'sync_trs':sync_time}
		output_data['rasa_nlu_data']['common_examples'].append(ex)
	
	f = codecs.open(output_file,'w', 'utf-8') #,'utf-8')
	json.dump(output_data,f, indent=4, ensure_ascii=False)
	f.close()
	
def get_allowable_list_of_slots_dict_from_file(annotation_file):
	f = codecs.open(annotation_file,'r','utf-8')
	data = json.load(f)
	f.close()
	
	output_format = {'entity':[],'intent':[],'value':[]}
	
	#we assume it's in this format for now
	output_format['intent'] = data['intents']
	for elem in data['slots']:
		output_format['entity'].append(elem['label'])
		output_format['value'].extend(elem['values'])
		
	return output_format
	
	
##################################
if __name__ == "__main__":

	import argparse
	parser = argparse.ArgumentParser()
	parser.add_argument("input_filename", help="the .trs file to parse")
	parser.add_argument("annotation_filename", help="the .json name for the annotation file")	
	parser.add_argument("--output_filename", help="the .json name for the output file", required=False)
	parser.add_argument("--default_intent", help="the default intent to use", required=False)
	
	args = parser.parse_args()
	
	
	input_filename = args.input_filename
	print('...input_file : ' + str(input_filename))
	annotation_filename = args.annotation_filename
	print('...annotation_file : ' + str(annotation_filename))
	if(args.output_filename):
		output_filename = args.output_filename
	else:
		output_filename = os.path.join(os.path.dirname(input_filename),os.path.splitext(os.path.basename(input_filename))[0] + '_annotator_tool.json')
	print('...output_file : ' + str(output_filename))
	
	if(args.default_intent):
		use_default_intent = args.default_intent
	else:
		use_default_intent = None
	
	utterances, sync_times = get_all_utterances(input_filename)
	allowable_list_of_slots_dict = get_allowable_list_of_slots_dict_from_file(annotation_filename)
	write_to_annotator_tool_format(utterances, sync_times, allowable_list_of_slots_dict, output_filename,use_default_intent)
	
	
	
	
	
	