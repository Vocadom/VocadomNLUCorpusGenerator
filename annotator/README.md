pre-annotator contains a automatic pre-annotator for VocADom/SWEET-HOME corpus in .trs format. The output requires a manual check in the tool_web_annotator. 

tool_web_annotator is an interface for annotating manually. Can be used to annotate from scratch (without pre-annotation). Please refer to the README in each folder to learn about the usage.
