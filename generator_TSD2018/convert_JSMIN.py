import json
import re






#TRI-CRF FILES

FILETESTLABTRI = open('minwoo_BIO.labels.test.data','w')
FILETESTVALTRI = open('minwoo_BIO.values.test.data','w')	


#ALIGNMENT ORTHO TEXT - CHAR POS			

def write_sent(infile,intent,sent):
		intent = re.sub('none','NONE',intent)
		infile.write("%s %s\n" % (intent,sent))
		

#SPLIT SENT
def splitsent(t):
			words = t.split('_')
			return words

def splitsent2(t):
			words2 = t.split(' ')
			return words2

def add_newline(infile):
	infile.write("\n")			

def write_slot_O(infilehadslot,string):
	infilehadslot.write("O ")

#TRICRF
def write_slot_tri(infiletri,infiletri2,slotstringdict,valstringdict,textline):
	tuples = []
	tuples2 = []
	dictlist=[]
	a = splitsent(textline)

	for word in a:
		pattern1=""
		b = splitsent2(word)
		wordb=str(word)

		pattern1=wordb
		
		
			

			
			

		for i in range(len(b)):
			


			if(i==0):

				if pattern1 in slotstringdict:

				
				
					
					stringkey = str(slotstringdict[wordb]) + "-B"
					stringval = str(valstringdict[wordb]) + "-B" 
					tuples.append((stringkey,b[i]))
					tuples2.append((stringval,b[i]))
				
				
				
					
					
					
				else:

					stringkey = "NONE"
					stringval = "NONE" 
					tuples.append((stringkey,b[i]))
					tuples2.append((stringval,b[i]))
					
					
					
			elif(i>0):

				if pattern1 in slotstringdict:

					stringkey = str(slotstringdict[wordb]) + "-I"
					stringval = str(valstringdict[wordb]) + "-I" 
					tuples.append((stringkey,b[i]))
					tuples2.append((stringval,b[i]))

					
									
				else:
		

					stringkey = "NONE"
					stringval = "NONE" 
					tuples.append((stringkey,b[i]))
					tuples2.append((stringval,b[i]))
	
	tuple_proc(tuples,infiletri,b)
	tuple_proc(tuples2,infiletri2,b)

def tuple_proc(tupl,filein,splitfile):
	abcounter=0
	i = 1		
	for a,b in tupl:

		
		abcounter+=1
		if(abcounter == 1):
			filein.write("%s word=%s word-1=<s> " % (a,b))
		
		elif(abcounter == 2):
			filein.write("%s word=%s word-2=<s> word-1=%s " % (a,b,splitfile[0]))
			
		else:
			filein.write("%s word=%s " % (a,b))
						
		j = -3
		for x in (tupl[i-3: i-1]):
			j += 1
			filein.write("word%d=%s " % (j,x[1]))
		if(i==len(tupl)):
			filein.write("word+1=</s>")
			
		k = 0
		for y in (tupl[i: i+2]):
			
			k += 1
			if(i<(len(tupl)-1)):			
				filein.write("word+%d=%s " % (k,y[1]))
			elif(i==(len(tupl)-1)):
				filein.write("word+%d=%s word+2=</s>" % (k,y[1]))
									
		filein.write("\n")	
		i += 1	
	filein.write("\n")


with open('test.json') as json_file:

	data=json.load(json_file)

	
	productions=data["rasa_nlu_data"]["common_examples"]
	for item in productions:
		slotstringdict={}
		valstringdict={}
		slotswords = []
		substdict={}

		if(len(item['intent'])) > 1:
			write_sent(FILETESTLABTRI,item['intent'],item['text'])

		write_sent(FILETESTVALTRI,item['intent'],item['text'])		

		textline = str(item['text'])

		origsentwords = splitsent2(textline)

		for z in item['entities']:							

		 	string=str(z['text'])
		 	string2 = "_" + string + "_"
		 	slot=str(z['entity'])
		 	value=str(z['value'])
		 	textline = re.sub(string,string2,textline)
		 	textline = re.sub('[ ]*_[ ]*','_',textline)
		 	textline = re.sub('[_]+','_',textline)
		 	textline = re.sub('_$','',textline)

		
			
				
				

		 	slotstringdict[string]=slot
		 	valstringdict[string]=value	


		
		write_slot_tri(FILETESTLABTRI,FILETESTVALTRI,slotstringdict,valstringdict,textline)
	
			
