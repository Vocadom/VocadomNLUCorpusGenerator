from nltk.featstruct import CustomFeatureValue	
from nltk.featstruct import UnificationFailure
from nltk.featstruct import FeatureValueSet
from nltk.sem.logic import Variable
import nltk
import re

_FEATURE_NAME_RE = re.compile(r'\s*([+-:]?)([^\s\(\)<>"\'\-=\[\],]+)\s*')

class ExclusiveTrueFeatureValue(CustomFeatureValue):
	""" New Feature Value. To be used for exclusively true unification.
		Can be specified via string as :FEAT={ON}
	"""
	def __init__(self,iter_of_features):
		if(isinstance(iter_of_features,Variable)):
			self.feat_list = iter_of_features
		
		else:
			#assert(len(iter_of_features) == 1)
			
			self.feat_list = set(iter_of_features)
		
	def unify(self, other):
		#if we're a variable
		if(isinstance(self.feat_list,Variable)):
			return other
			
		#if other is a variable	
		if(isinstance(other,Variable)):
			return self
			
		#if other is a variable (hidden in an ExclusiveTrueFeatureValue)
		if(isinstance(other,ExclusiveTrueFeatureValue) and isinstance(other.feat_list,Variable)):
			return self
		
		#if one is a string
		if(isinstance(self,ExclusiveTrueFeatureValue) and 
			isinstance(other,str)):
			if(other in self.feat_list):
				return other
			else:
				return UnificationFailure
			
		#both are Exclusively True Feature Values
		elif(isinstance(self,ExclusiveTrueFeatureValue) and 
			isinstance(other,ExclusiveTrueFeatureValue)):
			
			#if both sets are empty, we unify
			if(len(self.feat_list) == 0 and len(other.feat_list) == 0):
				return self
				
			#but if both sets are not empty - then we don't
			if(len(self.feat_list) == 0 or len(other.feat_list) == 0):
				return UnificationFailure 
				
			#in general, unify if one feat_list is the subset of the other.
			check = self.feat_list.issubset(set(other.feat_list))
			if(check):
				return self 
			
			check = self.feat_list.issuperset(set(other.feat_list))
			if(check):
				return other
				
			return UnificationFailure
			
		else:
			return UnificationFailure
		
		
	def __repr__(self):
		a = str(self.feat_list)
		a = re.sub('\'','',a)
		return a
		#return '(%s<x<%s)' % (self.low, self.high)
		
	def __eq__(self, other):
		#if the other one is a set
		if isinstance(other,FeatureValueSet):
			check = set(self.feat_list).union(set(other.feat_list)) == set(self.feat_list).intersection(set(other.feat_list))
			return check
			
		#if the other one is a ETFV
		elif isinstance(other, ExclusiveTrueFeatureValue):
			check = set(self.feat_list).union(set(other.feat_list)) == set(self.feat_list).intersection(set(other.feat_list))
		
			return check		
			
		#can't unify with anything else
		else:
			return False
		
	def __lt__(self, other):
		if not isinstance(other, ExclusiveTrueFeatureValue):
			return True
			
		s = sorted(self.feat_list)
		o = sorted(other.feat_list)
		return s[0] < o[0]
		
	def __hash__(self):
		return 1	
		
# The following function is pulled straight from ntlk and has only been modified to 
# allow for the ':' before a feature name to indicate that it is an ExclusiveTrueFeatureValue type feature
# instead of a regular set
def _read_partial_featdict(self, s, position, match,
                                reentrances, fstruct):
        # If there was a prefix feature, record it.
        if match.group(2):
            if self._prefix_feature is None:
                raise ValueError('open bracket or identifier', match.start(2))
            prefixval = match.group(2).strip()
            if prefixval.startswith('?'):
                prefixval = Variable(prefixval)
            fstruct[self._prefix_feature] = prefixval

        # If group 3 is empty, then we just have a bare prefix, so
        # we're done.
        if not match.group(3):
            return self._finalize(s, match.end(), reentrances, fstruct)

        # Build a list of the features defined by the structure.
        # Each feature has one of the three following forms:
        #     name = value
        #     name -> (target)
        #     +name
        #     -name
        while position < len(s):
            # Use these variables to hold info about each feature:
            name = value = None

            # Check for the close bracket.
            match = self._END_FSTRUCT_RE.match(s, position)
            if match is not None:
                return self._finalize(s, match.end(), reentrances, fstruct)

            # Get the feature name's name
            match = self._FEATURE_NAME_RE.match(s, position)
            if match is None: raise ValueError('feature name', position)
            name = match.group(2)
            position = match.end()

            # Check if it's a special feature.
            if name[0] == '*' and name[-1] == '*':
                name = self._features.get(name[1:-1])
                if name is None:
                    raise ValueError('known special feature', match.start(2))

            # Check if this feature has a value already.
            if name in fstruct:
                raise ValueError('new name', match.start(2))

            # Boolean value ("+name" or "-name")
            if match.group(1) == '+': value = True
            if match.group(1) == '-': value = False
			
			##NEW#################################
            if match.group(1) == ':': 
	
                match = self._ASSIGN_RE.match(s, position)
                if match:
                    position = match.end()
                    value, position = (
                        self._read_value(name, s, position, reentrances))
                # None of the above: error.
                else:
                    raise ValueError('equals sign', position)			
				
                value = ExclusiveTrueFeatureValue(value)

			#############################################
				
            # Reentrance link ("-> (target)")
            if value is None:
                match = self._REENTRANCE_RE.match(s, position)
                if match is not None:
                    position = match.end()
                    match = self._TARGET_RE.match(s, position)
                    if not match:
                        raise ValueError('identifier', position)
                    target = match.group(1)
                    if target not in reentrances:
                        raise ValueError('bound identifier', position)
                    position = match.end()
                    value = reentrances[target]

            # Assignment ("= value").
            if value is None:
                match = self._ASSIGN_RE.match(s, position)
                if match:
                    position = match.end()
                    value, position = (
                        self._read_value(name, s, position, reentrances))
                # None of the above: error.
                else:
                    raise ValueError('equals sign', position)

            # Store the value.
            fstruct[name] = value

            # If there's a close bracket, handle it at the top of the loop.
            if self._END_FSTRUCT_RE.match(s, position):
                continue

            # Otherwise, there should be a comma
            match = self._COMMA_RE.match(s, position)
            if match is None: raise ValueError('comma', position)
            position = match.end()

        # We never saw a close bracket.
        raise ValueError('close bracket', position)		
		
#And then actually update ntlk to allow for this to be possible
nltk.featstruct.FeatStructReader._read_partial_featdict = _read_partial_featdict
nltk.featstruct.FeatStructReader._FEATURE_NAME_RE = _FEATURE_NAME_RE 
nltk.featstruct.FeatStructReader._START_FDICT_RE = re.compile(r'(%s)|(%s\s*(%s\s*(=|->)|[+-:]%s|\]))' % (
		nltk.featstruct.FeatStructReader._BARE_PREFIX_RE.pattern, nltk.featstruct.FeatStructReader._START_FSTRUCT_RE.pattern,
		nltk.featstruct.FeatStructReader._FEATURE_NAME_RE.pattern, nltk.featstruct.FeatStructReader._FEATURE_NAME_RE.pattern))		