# util.py
import re
import nltk
from nltk.featstruct import Feature
from nltk.featstruct import FeatStruct
from nltk.sem.logic import Variable
from collections import defaultdict
from nltk.featstruct import unify
import util_nltk_modifications

DEBUG = 3

###FOR PRINTING ######################################	
def to_single_line_str(l):
	""" Convert a production or a list of symbols to a single printable line 
		: returns : a single line printable string 
	"""
	s = str(l)
	s = s[1:] if s[0] in ['[','('] else s
	s = s[0:-1] if s[-1] in [']',')'] else s
	return re.sub('[\s]+', ' ', s)
	
def fstr_to_single_line_str(l,print_depth):
	m = re.sub('[\n]+', '\n' + '|'*print_depth, str(l))
	return m 
	
def print_with_depth(elem,print_depth,debug_level=3, LOCAL=None):
	global DEBUG
	if(LOCAL):
		DEBUG_TO_USE = LOCAL
	else:
		DEBUG_TO_USE = DEBUG
		
	if(DEBUG_TO_USE and DEBUG_TO_USE == debug_level ):
		print('|'*1*print_depth + str(elem))
#######################################################	

def write_to_file(filename, content):
	""" Write s to file.
	"""
	f = open(filename,'w')
	f.write(content)
	f.close()	

def filter_of_compile_flags(lines,compile_flags={}):
	curr_section = None
	curr_lines = []
	new_new_lines = []
	depth = 0
	did_find_one = False
	for line in lines:
		#print(line)
		#if we're not in a section, look for the start tag
		#if(curr_section == None):
		match_start = re.match('\%if ([\w]+)\%',line)
		match_end = re.match('\%end if\%',line)
		if(match_start):
			did_find_one = True
			depth += 1
			#print('match_start')
			#print(depth)
			#we've found it! start a new section
			if(depth == 1):
				new_new_lines.extend(curr_lines)
				curr_lines = []
				curr_section = match_start.group(1)
			else:
				curr_lines.append(line)	
		elif(match_end):
			depth -= 1
			if(depth == 0):
				#print('match_end')
				#print(depth)
				#we've found it! now decide whether to add or not
				if(curr_section in compile_flags and compile_flags[curr_section]):
					new_new_lines.extend(curr_lines)
				#either way restart the process
				#print('---')
				#print(curr_lines)
				curr_lines = []
				curr_section = None
				#print(curr_section)
				#print(compile_flags)
			else:
				curr_lines.append(line)				
		else:
			if(curr_section == None):
				#still not in a section
				new_new_lines.append(line)
			else:
				curr_lines.append(line)
		#we're still in a section, look for the end tag
		# #else:
		# match_end = re.match('\%end if\%',line)

		# else:
			# curr_lines.append(line)	
					
	return new_new_lines, did_find_one

def read_multiple_files(FILES,START,compile_flags={}):
	""" Read multiple fcfg files into a single string
		Also add the start command.
		Uses utf-8 encoding.
		# indicate comments.
		
		: param FILES : list of strings, filenames of the fcfg files
		: param START : string, start character
		: returns : string, the compiled files
	"""
	content = '% start ' + START + '\n'
	for file in FILES:
		f = open(file,'r', encoding='utf-8')
		#data = f.read().decode("UTF-8") 
		lines = [line.split('#')[0].strip() for line in f.readlines()]
		#print(lines)
		lines = list(filter(lambda x: x.strip(), lines))
		
		#print('...reading file')
		new_lines = []
		curr_line = []
		i = 0
		while i < len(lines):
			l = lines[i].strip()
			if(l[-1] in [',','\\','>']):
				if(l[-1] == '\\'):
					l = l[0:-1]
				curr_line.append(l)
			else:
				if(curr_line != []):
					curr_line.append(l)
					new_lines.append(' '.join(curr_line))
					curr_line = []
				else:
					new_lines.append(l)
				#curr_line.append(l)
			i+=1
		if(curr_line != []):
			new_lines.append(' '.join(curr_line))
		
		#now filter
		did_find_one = True
		new_new_lines = new_lines
		while(did_find_one):
			new_new_lines, did_find_one = filter_of_compile_flags(new_new_lines,compile_flags)
				
		
		prefix = '\n'.join(new_new_lines)
		#print(prefix)
		#print('-----')
		f.close()
		content += prefix + '\n'
	return content
	
def get_all_prod_of_type_starts_with(grammar,x):
	""" Given a production (ie a rule like S-> NP VP) in a grammar, return all rules which 
		have a lhs symbol (ie S) starting with "x" 
		ex. if x = "Start", then any rule for symbol "StartBlahBlahBlah" will be included in the returned rules
		
		: param grammar : an nltk grammar
		: param x : string, prefix for lhs symbol of all rules to return
		: returns : list, productions (ie rules) 
	"""
	x_productions = [prod for prod in grammar.productions() if re.match(x, prod._lhs.get(Feature('type')))]
	return x_productions	
	
def sort_prod_based_on_lhs_feature(prods, feat_to_sort_by):
	types = {}
	for prod in prods:
		device_type = prod._lhs.get(feat_to_sort_by)
		if(device_type in types):
			types[device_type].append(prod)
		else:
			types[device_type] = [prod]
	return types
	
def replace_featstructa_elems_with_elems_from_b(fsa,fsb):
	""" Replace all values in fsa with values from fsb if the key exists.
	Otherwise use the existing value.
	Do not add keys from fsb that are not in fsa
	"""
	newdict = {}
	for elem in fsa:
		if(elem in fsb):
			newdict[elem] = fsb[elem]
		else:
			newdict[elem] = fsa[elem]
	
	return FeatStruct(newdict) 
	
def clean_featstruct_of_x(fsa,x,deep=True):
	""" Remove string feature "x" from a feature struct.
		Don't remove anything if it's not there. (But still return a copy).
				
		: param fsa : a feature struct from which to remove features
		: param deep : 	True - search all layers of the fsa, 
						False - only consider top level features 
		: returns : a new featstruct without the removed features
	"""
	
	cfsa = fsa.copy()
	try:
		cfsa.pop(x)
	except KeyError as e:
		pass
			
	if(deep):
		for elem in fsa:
			if(isinstance(fsa[elem],FeatStruct)):
				sub_copied_unif = clean_featstruct_of_x(cfsa[elem],x,deep)
				cfsa[elem] = sub_copied_unif
	return cfsa
	
def clean_unification_of_type_and_id(fsa):
	""" Remove the "type" and "ID" features from a feature struct. 
		
		: param fsa : a feature struct from which to remove features
		: returns : a new featstruct withut the removed features
	"""
	copied_unif = fsa.copy()
	copied_unif.pop(Feature('type'),None)
	copied_unif.pop('ID',None)
	return copied_unif
	
def new_featstruct_as_str(d):
	""" Create the string of a new featstruct from a dictionary of name:value pairs.
		Values may also be dictionaries in order to have sub-feature structs
		
		: param d: a dictionary representing name:value features of a feature struct
		: returns : str, the string representation of the featstruct
	"""
	s = '['
	for i,key in enumerate(sorted(d)):
		if(i>0):
			s+= ','
		if(isinstance(d[key], dict)):
			s += '%s=%s' % (key,new_featstruct_as_str(d[key]))
		elif(isinstance(d[key],util_nltk_modifications.ExclusiveTrueFeatureValue)):
			s += ':%s=%s' % (key, str(d[key]))
		else:
			s += '%s=%s' % (key, d[key])
	s += ']'
	return s	
	
def new_single_symbol_as_str(symbol,featstruct):
	""" Create a string for a symbol in a production (ex. 'S[Feat=val]')
		
		: param symbol : the str for the symbol (ex. 'S')
		: param featstruct : either str or dict, representing the feature structure. if a dict, it will be converted to a string
		
		: returns : str, combining the symbol and its featurestruct
	"""
	if(isinstance(featstruct,str)):
		return symbol + featstruct
	elif(isinstance(featstruct,nltk.grammar.FeatStructNonterminal)):
		return symbol + new_featstruct_as_str(featstruct)
	elif(isinstance(featstruct,dict)):
		return symbol + new_featstruct_as_str(featstruct)
	else:
		raise(ValueError)	
		
def new_prod_as_str(lhs,rhs):
	""" Create a new production given a lhs and a rhs (ex. S[Feat=val] -> S1[] S2[Feat1=val2])
	
		: param lhs : str or dict, if dict: key:value indicate the symbol: feat_struct, where the featstruct may be either a string or a dictionary itself of the form accepted by new_single_symbol_as_str
		: param rhs : str or dict, if dict: key:value are of the same format as lhs, but they represent each element in the rhs of the production individually
	"""
	
	#print(type(lhs))
	#print(type(rhs))
	#input()	
	if(isinstance(lhs,dict)):
		assert(len(lhs) == 1)
		#print(lhs.items())
		for elem in lhs.items():
			print(type(elem[0]))
			print(type(elem[1]))
			for e in elem[1]:
				print(type(e))
			print('---')
		#input()
		elems = [new_single_symbol_as_str(key,value) for key,value in sorted(lhs.items())]
		lhs_str = elems[0]
		
	elif(isinstance(lhs,str)):
		lhs_str = lhs
		
	if(isinstance(rhs,dict)):
		elems = [new_single_symbol_as_str(key,value) for key,value in sorted(rhs.items())]
		rhs_str = ' '.join(elems)
		
	elif(isinstance(rhs,str)):
		rhs_str = rhs
	
	return '%s -> %s' % (lhs_str,rhs_str)		
	
	
def get_variable_to_feature_mapping(fs1):
	""" Return the set of all variable feature values (ie ?n) in a feature set, 
		regardless of depth and their associated features as a dict
		ex. F[A=?n,B=5,C=[Y=?x,Z=?n]] -> {?n:[A,Z],?x:[Y]]
		ex. F[A=?n,B=5,C=[Y=?x,Z=?n,A=?n]] -> {?n:[A,A,Z],?x:[Y]]
		
		: param fs1 : feature structure
		: returns : dictionary of variable:[feat1,feat1...] 
	"""
	vardict = defaultdict(list)
	for feat in fs1:
		value = fs1[feat]
		
		if(isinstance(value,Variable)):
			vardict[value].append(feat)
			vardict[value].sort()
			
		elif(isinstance(value,FeatStruct)):
			subdict = get_variable_to_feature_mapping(value)
			for var in subdict:
				vardict[var].extend(subdict[var])
				vardict[var].sort()
			
	return vardict
	
def get_unique_variable_to_feature_mapping(fs1):
	""" Return the mapping from var->feat 
		But only allow each var to have a unique feature. 
		Throw an error if the same variable is used more than once in the same feature struct
		
		ex. F[A=?n,B=5,C=[Y=?x]] -> {?n:A,?x:Y}
		ex. F[A=?n,B=5,C=[Y=?x,Z=?n,A=?n]] -> error! (should be {?n:[A,A,Z],?x:Y, but ?n has multiple values})
		
		: param fs1 : FeatStruct
		: returns :  a dictionary of var:feat pairs for every variable in the FeatStruct
	"""
	varvals = get_variable_to_feature_mapping(fs1)
	varval = {}
	for var_name in varvals:
		feat_list = varvals[var_name]
		
		if(len(feat_list) > 1):
			raise(ValueError('bad format: variable name appears multiple times in formula'))
		else:
			varval[var_name] = feat_list[0]
			
	return varval	
	
def replace_feat_with_new_name(old_name,new_name,fs):
	""" Replace all instances of feature old_name in FeatStruct fs with 
		new features named new_name.
		Note that this is done *in place* (does not return a copy)
		
		ex. fs=F[X=n] old_name=X new_name=Y -> fs=F[Y=n]
	"""
	new_fs = fs.copy()
	for feat in new_fs:
		if(feat == old_name):
			fs[new_name] = fs[old_name]
			fs.pop(old_name)
		elif(isinstance(fs[feat],FeatStruct)):
			replace_feat_with_new_name(old_name,new_name,fs[feat])	

def get_all_feat_values(fs):
	""" Get a dictionary of all feat-> value mappings in a feature struct
		: param fs : feature structure to parse
		: returns : dict, key=feature names, val=list, value of that feature (regardless of depth)
	"""
	feat_vals = defaultdict(list)
	
	for feat in fs:
		val = fs[feat]
		
		#value is a feature struct, parse the sub-structure
		if(isinstance(val,FeatStruct)):
			sub_feat_values = get_all_feat_values(val)
			for elem in sub_feat_values:
				#add these new base values to the master list
				feat_vals[elem].extend(sub_feat_values[elem])
		#value is a base value, add it
		else:
			feat_vals[feat].append(val)
			
	return feat_vals

def get_all_keys_from_list_of_dicts(l):
	""" Collect all the keys in a list of dictionaries
		Only considers 1-level deep!!!
		
		: param l : list of dictionaries
		: returns : set, all keys in all dictionaries
	"""
	new_set = set()
	for d in l:
		new_set.update(d.keys())
	return new_set

def create_alias_to_var_mapping(vars1_2_feat, each_vars2_2_feat):
	""" Create actual alias names from a set of variables
	
	: param vars1_2_feat : dict, ?a->[feat1]
	: param each_vars2_2_feat : list of dicts, ?a->[feat2]
	: returns : dict, 'alias'->?a
	"""
	#...this is all the variables in itemrest
	vars_in_2 = get_all_keys_from_list_of_dicts(each_vars2_2_feat)
		
	#...this is the set of overlap between item1 and itemrest
	overlapping_vars = set(vars1_2_feat.keys()).intersection(vars_in_2)

	alias_dict = {}
	for var in overlapping_vars:
		symbol_1_name = vars1_2_feat[var]
		rest_symbol_name = '_'.join([str(i) + '-' + symbol[var] for i,symbol in enumerate(each_vars2_2_feat) if var in symbol.keys()])
		alias_name = 'alias_%s_%s' % (str(symbol_1_name),str(rest_symbol_name))
		assert(alias_name not in alias_dict)
		alias_dict[alias_name] = var
	return alias_dict
	
def replace_featstruct_with_alias(alias_dict, fs_to_fill, vars_2_feat):
	copy_f1 = fs_to_fill.copy()
	
	for alias_name in alias_dict:
		#the variable name ?b
		var_name = alias_dict[alias_name]
		
		#the associated feature; B->?b (ie B)
		#print(vars_2_feat)
		#print(fs_to_fill)
		if(var_name in vars_2_feat):
			feat_name_1 = vars_2_feat[var_name]
		
			replace_feat_with_new_name(feat_name_1,alias_name,copy_f1)
		
	return copy_f1
		
def replace_featstructs_with_aliases(alias_dict, item1_filled, itemrest_filled, vars1_2_feat, each_vars2_2_feat):
	
	copy_f1 = replace_featstruct_with_alias(alias_dict, item1_filled, vars1_2_feat)
	
	copy_f2 = itemrest_filled.copy()
	for vars2_2_feat in each_vars2_2_feat:
		copy_f2 = replace_featstruct_with_alias(alias_dict, copy_f2, vars2_2_feat)
	
	return copy_f1, copy_f2
	
	
def generate_alias(item1_orig,item1_filled,itemrest_orig,itemrest_filled):
	""" Generates an alias for two featstructs and returns the unification (using the alias) and 
		the mapping from the original variables to the alias.
		
		item1_orig: a featstruct, the "unfilled" feature struct associated with item1
		item1_filled: a featstruct, the filled featstruct associated with item1
		itemrest_orig: a list of featstructs, the unfilled feature structs associated with item2:itemT
		itemrest_filled: a featstruct, the filled featstruct associated with item2:itemT

		returns: 
		the unification of item1_filled and itemrest_filled with aliases learned from the original featstructs
		a mapping form the aliases to the original variables so that the original variables can be recovered. 		
	"""
	
	#this is just to help with printing
	DEBUG_ALIAS = False
	if(DEBUG_ALIAS):
		LOCAL=4
	else:
		LOCAL=0
	print_with_depth('\n\n...generating alias', 0, debug_level=4, LOCAL=LOCAL)
	print_with_depth('orig:', 0, debug_level=4, LOCAL=LOCAL)
	print_with_depth(item1_orig, 0, debug_level=4, LOCAL=LOCAL)
	print_with_depth('___', 0, debug_level=4, LOCAL=LOCAL)
	print_with_depth(itemrest_orig, 0, debug_level=4, LOCAL=LOCAL)
	print_with_depth('---', 0, debug_level=4, LOCAL=LOCAL)
	print_with_depth(item1_filled, 0, debug_level=4, LOCAL=LOCAL)
	print_with_depth('___', 0, debug_level=4, LOCAL=LOCAL)
	print_with_depth(itemrest_filled, 0, debug_level=4, LOCAL=LOCAL)
	print_with_depth('=========', 0, debug_level=4, LOCAL=LOCAL)		
	
	item1_orig_copy = item1_orig.copy()
	
	#Step 1: find all the variables originally in item1 and itemrest
	vars1_2_feat = get_unique_variable_to_feature_mapping(item1_orig_copy) #?a: AGR
	each_vars2_2_feat = [get_unique_variable_to_feature_mapping(item.copy()) for item in itemrest_orig]
	#####print('vars1_2_feat' + str(vars1_2_feat))
	#####print('each_vars2_2_feat' + str(each_vars2_2_feat))
	#####print('----')
	
	#Step 1.5: we only need to alias for variables which appear in both 1 and 2 because 2 has already been properly dealt with (so get that set...)
	#Step 2: get all the variable names associated with those values and create their aliases
	alias_dict = create_alias_to_var_mapping(vars1_2_feat, each_vars2_2_feat)
	print_with_depth(alias_dict, 0, debug_level=4, LOCAL=LOCAL)
	
	#Step 3: now clean the final featstructs of those variables and replace them with the aliases
	copy_f1, copy_f2 = replace_featstructs_with_aliases(alias_dict, item1_filled, itemrest_filled, vars1_2_feat, each_vars2_2_feat)
			
	print_with_depth('-------------', 0, debug_level=4, LOCAL=LOCAL)
	print_with_depth(copy_f1, 0, debug_level=4, LOCAL=LOCAL)
	print_with_depth('---', 0, debug_level=4, LOCAL=LOCAL)
	print_with_depth(copy_f2, 0, debug_level=4, LOCAL=LOCAL)
	
	copy_f1 = clean_unification_of_type_and_id(copy_f1)
	copy_f2 = clean_unification_of_type_and_id(copy_f2)
	
	
	aliased_unification = unify(copy_f1,copy_f2,rename_vars=False)
	print_with_depth('---unif---',0, debug_level=4, LOCAL=LOCAL)
	print_with_depth(aliased_unification, 0, debug_level=4, LOCAL=LOCAL)
	return aliased_unification, alias_dict
	
def repopulate_alias(unif_to_repopulate,alias_dict,alias_vals):
	copied_unif = unif_to_repopulate.copy()
	# print('---')
	# print(unif_to_repopulate)
	# print(alias_dict)
	# print(alias_vals)
	rev_alias_dict = {value:key for key,value in alias_dict.items()}
	for feat in copied_unif:
		var_name = copied_unif[feat]
		if(var_name in rev_alias_dict):
			alias_name = rev_alias_dict[var_name]
			if(alias_name in alias_vals):
				copied_unif[feat] = alias_vals[alias_name]
			else:
				for elem in alias_vals:
					if(isinstance(alias_vals[elem],FeatStruct)):
						feat_val, _ = get_feat_value(alias_name,unif_to_repopulate)
						copied_unif[feat] = feat_val
		if(isinstance(var_name,FeatStruct)):
			sub_copied_unif = repopulate_alias(var_name,alias_dict,alias_vals)
			copied_unif[feat] = sub_copied_unif
	return copied_unif

def strip_of_non_existant_elems_in_a(a,b):
	""" Remove all features in featstruct b which do not appear in featstruct a-
		...adding all features missing from b
		...removing all features that are not in a
	"""
	if(isinstance(a,Variable)):
		return b
		
	new_item = nltk.grammar.FeatStructNonterminal()
	for elem in b:
		if(isinstance(b[elem],FeatStruct)):
			if(elem in a):
				nn = strip_of_non_existant_elems_in_a(a[elem],b[elem])
				new_item[elem] = nn
		else:
			if(elem in a):
				if(b[elem] == None):
					new_item[elem] = a[elem]
				else:
					new_item[elem] = b[elem]
				
	for elem in a:
		if(not elem in new_item):
			new_item[elem] = a[elem]
			
	new_item.freeze()
	return new_item

def get_feat_value(feat2replace,fs,did_replace=False):
	feat_val = None

	for feat in fs:

		if(feat == feat2replace):
			if(did_replace):
				raise(ValueError, 'feat appears more than once')
			feat_val = fs[feat]
			did_replace = True			
		elif(isinstance(fs[feat],FeatStruct)):

			feat_val_sub,did_replace_sub = get_feat_value(feat2replace,fs[feat],did_replace)
			if(feat_val_sub != None):
				feat_val = feat_val_sub
				did_replace = did_replace_sub
	
	return feat_val,did_replace	
							
