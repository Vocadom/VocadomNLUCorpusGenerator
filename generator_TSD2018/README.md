*=============================*
===============================
*=ARTIFICIAL CORPUS GENERATOR=*
===============================
*=============================*

0.PRELIMINARY
-------------


-Version prepared for TSD article of 2018 March 28, 'Towards a French smart-home voice command corpus: design and NLU experiments'

Subfolder with_prop_names : is a version of the grammar that generates keyword proper namesm such as, 'vocadom'.\
Subfolder with_keyword : is a version of the grammar that generates the keyword 'KEYWORD' for all generated utterances.

-Developed under python anaconda 3.6.4 \
scripts should be run in python 3.6

1.BATCH FILE TO RUN THE TOOL
----------------------------

***************************
***************************
_GEN_ART_CORP1.sh

COMMAND TO RUN:

>./_GEN_ART_CORP1.sh

In order to convert a test file from json format into Att-RNN format use convert_ATTRNN.py script
command
>python convert_ATTRNN.py
(input file to specify within script)

In order to convert a test file from json format into Tri-CRF format use convert_JSMIN.py script
command
>python convert_JSMIN.py
(input file to specify within script)

***************************
***************************


2.CONTENTS BATCH FILE
---------------------

-runs python script ART_CORPGEN1.py \
&nbsp; &nbsp; script generates the corpus in 3 formats \
-runs perl commands on output files in rasa format \
&nbsp; &nbsp; modifies output of rasa format 


3.(REQUIRED) FILES FOR RUNNING _GEN_ART_CORP1.sh
------------------------------------------------
```bash
_GEN_ART_CORP1.sh

amiqual_commands.fcfg
amiqual_specification.fcfg
generic_smarthome_specification.fcfg
vocab.fcfg

ART_CORPGEN1.PY
run.py
util.py
util_nltk_modifications.py
```

4.DESCRIPTION OF (REQUIRED) FILES FOR RUNNING _GEN_ART_CORP1.sh
---------------------------------------------------------------

fcfg files for NLTK feature grammar 

amiqual_commands.fcfg \
&nbsp; &nbsp;  top level fcfg file. \
&nbsp; &nbsp;  It defines the rules. 

amiqual_specification.fcfg \
&nbsp; &nbsp;  identifies the objects present in the smart-home and the properties associated with the objects 

generic_smarthome_specification.fcfg \
&nbsp; &nbsp;  defines which actions can be applied to which objects 
 
vocab.fcfg \
&nbsp; &nbsp;  defines all words of the grammar 

For a detailed description of the fcfg files, see technical_report in /hephaistos/archives/Old_members/archiv_stagiaires/Stefania.Raimondo2017/GrenobleSmartHome/report_technical$ 

ART_CORPGEN1.PY 

python script that generates the feature grammar and semantically annotatated domotic corpus (test+train files) in 3 formats: \
&nbsp; &nbsp; Rasa-NLU format \
&nbsp; &nbsp; Tri-CRF format \
&nbsp; &nbsp; Att-RNN format

5.OUTPUT FILES
---------------


The a training set (TRAIN), dev set (TEST), and validation test file (TEST2) are automatically generated after running the script. The output files are in 3 formats, to use for traning models using the tools, RASA-NLU, Tri-CRF, and Att-RNN format. 

A. RASA-NLU format 


DOMOTIC_TRAIN_rasa.json \
DOMOTIC_TEST_rasa.json \
DOMOTIC_TEST2_rasa.json 

Train and test files to run RASA-NLU 

example format: 


```bash
{
  "rasa_nlu_data": {
    "common_examples": [
      {
        "text": "KEYWORD appelle médecin ",
        "entities": [
          {
            "start": 8,
            "end": 15,
            "entity": "action",
            "value": "CONTACT",
            "text": "appelle"
          },
          {
            "start": 16,
            "end": 23,
            "entity": "person-occupation",
            "value": "docteur",
            "text": "médecin"
          }
        ],
        "intent": "contact",
        "ordre_id": "2"
      },
...

```


B. Tri-CRF format  

Train and test files with slots and value to run Tri-CRF tool  

minwoo_BIO.labels.train.data  \
minwoo_BIO.labels.test.data  \
minwoo_BIO.labels.test2.data  

minwoo_BIO.values.train.data  \
minwoo_BIO.values.test.data  \
minwoo_BIO.values.test2.data  


example format:  \
slots  
```bash
contact KEYWORD appelle médecin 
NONE word=KEYWORD word-1=<s> word+1=appelle word+2=médecin 
action-B word=appelle word-2=<s> word-1=KEYWORD word+1=médecin word+2=</s>
person-occupation-B word=médecin word-2=KEYWORD word-1=appelle word+1=</s>

...
```


example format: \
values 

```bash
contact KEYWORD appelle médecin 
NONE word=KEYWORD word-1=<s> word+1=appelle word+2=médecin 
CONTACT-B word=appelle word-2=<s> word-1=KEYWORD word+1=médecin word+2=</s>
docteur-B word=médecin word-2=KEYWORD word-1=appelle word+1=</s>
...
```


C. Att-RNN format 


train.label \
train.seq.in \
train.seq.out \
trainval.seq.out

test.label \
test.seq.in \
test.seq.out \
testval.seq.out

test2.label \
test2.seq.in \
test2.seq.out \ 
testval2.seq.out

Example format  

.label (intent class)  \
contact  

.in (ortho input)  \
KEYWORD appelle samu  

seq.out (slot label)  \
O B-action B-organization   

val.seq.out (value label)  \
O B-CONTACT B-samu  
```


