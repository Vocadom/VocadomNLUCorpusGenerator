import re
INFILE = open('_ALL1_NONE.json','r')
OUTFILE = open('converted.json','w')

replacements = {'allo':'allô',
				'allumier':'allumer',
				'berenio':'KEYWORD',
				'berénio':'KEYWORD',
				'bérénio':'KEYWORD',
				'bein':'ben',
				'besse':'baisse',
				'chant':'KEYWORD',
				'chanticou':'KEYWORD',
				'chaticou':'KEYWORD',
				'coût':'KEYWORD',				
				'cirrsu':'KEYWORD',
				'cirrus':'KEYWORD',
				'cirus':'KEYWORD',
				'cyrus':'KEYWORD',								
				'citrus':'KEYWORD',	
				'dela':'de la',							
				'di':'dis',	
				'divocadom':'dis KEYWORD',
				'eh':'éh',										
				#'dix':'dis',
				'échifi':'é KEYWORD',
				'écirrus':'é KEYWORD',
				'estia':'KEYWORD',
				'élista':'KEYWORD',				
				'éteigner':'éteignez',				
				'faus':'faut',
				'fenair':'fenêtres',
				'hestia':'KEYWORD',
				'ichefic':'KEYWORD',				
				'ichefix':'KEYWORD',
				'ichéfix':'KEYWORD',				
				'lex':'les',
				'locadom':'KEYWORD',	
				'm\'sieur':'KEYWORD',						
				'mecadom':'KEYWORD',	
				'messir':'KEYWORD',							
				'messire':'KEYWORD',
				'minouche':'KEYWORD',								
				'ouvrer':'ouvrez',
				'plait':'plaît',
				#'qu\'est-ce':'est-ce',				
				'rez-de-chaussé':'rez-de-chaussée',				
				'score':'store',	
				'séraphim':'KEYWORD',
				'seraphim':'KEYWORD',				
				'séraphin':'KEYWORD',
				'sirius':'KEYWORD',
				'sirus':'KEYWORD',
				'stars':'stores',
				'stope':'stoppe',
				'stop':'stoppe',
				'teraphim':'KEYWORD',				
				'téraphim':'KEYWORD',
				'téraphin':'KEYWORD',
				'térapim':'KEYWORD',
				'ulysse':'KEYWORD',
				'vesta':'KEYWORD',
				'verifierai':'vérifierai',									
				'vocadm':'KEYWORD',				
				'vocadom':'KEYWORD',
				'vocadum':'KEYWORD'
				}





def replace(match):
    return replacements[match.group(0)]


for bline in INFILE:
	if re.search('\"text\":',bline):
		bline = bline.lower()
	line = bline.rstrip()
	#line = re.sub(r"\ballo\b","\bXXXXXXXXXXXX\b",line)
	#OUTFILE.write("%s\n" % (line))
	
	
	


# notice that the 'this' in 'thistle' is not matched 
	line = (re.sub('|'.join(r'\b%s\b' % re.escape(s) for s in replacements), replace, line) )
	OUTFILE.write("%s\n" % (line))	
