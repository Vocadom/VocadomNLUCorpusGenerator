import nltk
import codecs
from util import *
import sys
import re
from util_nltk_modifications import *
from nltk.grammar import Nonterminal
import nltk.featstruct
from nltk.sem.logic import Variable
import pprint

source=open("source.txt",mode='w',encoding='utf8')
target=open("target.txt",mode='w',encoding='utf8')




#from nltk.parse.generate import generate
START = 'COMMAND'
#these are the files you want to read
FILES = ['amiqual_commands.fcfg',
		'amiqual_specification.fcfg',
		'generic_smarthome_specification.fcfg',
		'vocab.fcfg']

#this determines which print statements actually get printed
DEBUG=3
	
	
def fake_the_device_groups(grammar):
	""" Produce new productions (ie S-> VP NP rules) for SpecDeviceGroup, which can be inferred from the 
		SpecDevice productions already in the grammar.
		(This allows you to not have to write them yourself.)
		It actually produces two new rules:
		
		1.
		SpecDeviceXGroup[AGR=[PLUR=plur,VOWEL=?v,GENDER=?g],Location=Y] -> 
			WordDeviceX[AGR=?a]
		
		where X is the device type (ex. Light) and Y is the location (where multiple of these devices are found)
		
		2. DeviceX[Z] -> SpecDeviceXGroup[AGR=?a,Location=?l]
	
		where Z is the same as for the DeviceX -> SpecDeviceX rules that this was derived from 
		
		: param grammar : the existing grammar
		: returns : a list of productions as strings
	"""
	prods = get_all_prod_of_type_starts_with(grammar, 'SpecDevice')
	new_prods = []
	
	#separate prods into their respective device tyeps
	device_types = sort_prod_based_on_lhs_feature(prods, Feature('type'))	
	for devtype in device_types:
		locs = {}
		locs = sort_prod_based_on_lhs_feature(device_types[devtype],'Location')		
		newtype = devtype + 'Group'
		
		for loc in locs:
			if(len(locs[loc]) < 2):
				continue

			#pick one of the productions that has this location
			sample_prod = locs[loc][0]

			d = {'Location':{k:v for k, v in sample_prod._lhs['Location'].items()},
				'AGR':'[PLUR=plur,VOWEL=?v,GENDER=?g]'}

			str = new_prod_as_str({newtype:d},
									sample_prod._rhs.__str__()[1:-2])
									
			new_prods.append(str)
			
			#now also add the new device rule
			base_dev_type = devtype[4:] #get rid of the Spec
			sub_prods = get_all_prod_of_type_starts_with(grammar, base_dev_type)

			for sub_prod in sub_prods:

				new_lhs= {sub_prod._lhs[Feature('type')]:clean_featstruct_of_x(sub_prod._lhs,Feature('type'),True)}
				new_rhs = {newtype:clean_featstruct_of_x(sub_prod._rhs[0],Feature('type'),True)}
				str = new_prod_as_str(new_lhs,new_rhs)
				new_prods.append(str)
	

	return new_prods

CACHE = {}
def add_to_cache(item, cache_val):
	global CACHE
	if(item in CACHE):
		if(not cache_val in CACHE[item]):
			CACHE[item].append(cache_val)
	else:
		CACHE[item] = []
		CACHE[item].append(cache_val)		
	
def get_generator(grammar):
	""" Generate all possible productions given a grammar. 
		THIS IS YOUR MAIN GENERATION FUNCTION
		: returns : the generator that will yield all sentences in the grammar
	"""
	#get the start symbol
	start = grammar.start()
	
	#run to maximum system depth
	depth = sys.maxsize
	
	#generate all productions for the start symbol
	iter = _generate_all(grammar, [start], depth)
	
	#return the generator
	return iter		
		
def _generate_one(grammar, item, depth,print_depth= 0):
	""" Fully generate a single element in the grammar
		ie given a RHS symbol, possibly with a partially initialized feature struct, 
		get all possible fully specified replacements for this symbol
		: yields: (all of) a word expansion of item, and a unification for that item
	"""
	
	if depth > 0:

		if isinstance(item, Nonterminal):
			#If we've done this already, just use what we put in the cache.
			if(item in CACHE):
				#print_with_depth('Using cache...', print_depth,3)
				for elem in CACHE[item]:
					yield(elem)
			else:
				#If it's not terminal... get all the possible expansions...
			
				#...only consider the productions starting with this item
				for prod in grammar.productions(lhs=item):
					#print_with_depth(str(prod),print_depth,3)
					#...only match productions which meet the criteria (feat-struct must match)
					
					#rename the variables since they come from different productions
					#unifying LHS of the new production with our original symbol
					#if for example, the rule is only for PLUR=plur and our symbol already has PLUR=sing then 
					#this rule isn't useful to us
					unification = unify(item,prod._lhs,rename_vars = True)
					if(unification):

						#now we need to replace this single itme with its expanded production rule
						for (frag,frag_unif) in _generate_all(grammar, prod.rhs(), depth-1,print_depth+1):
							copied_unification = clean_unification_of_type_and_id(unification)
							
							#now unify the new expansion with the current lhs info (if possible)
							if(frag_unif != None):
								#just do a basic unification for now, to make sure all the filled elements are correct
								frag_unif = clean_unification_of_type_and_id(frag_unif)
								new_unif = unify(copied_unification,frag_unif,rename_vars = 
	 True)
								unifies = new_unif != None
							
							else:
								#no featstruct form the rhs meant it was a terminal and thus definitely unifies
								new_unif = copied_unification
								unifies = True
							
							if(unifies):
								
								newfeat = replace_featstructa_elems_with_elems_from_b(unification,new_unif)
								
								if(frag_unif != None):
									####print('++++')
									####print(item)
									####print('---')
									####print(copied_unification)
									####print('---')
									####print(newfeat)
									####print('---')
									####print(prod.rhs())
									####print('---')
									####print(frag_unif)
									####print('++++')
									
									#now do the aliased unification in order to propagate the values correctly to the LHS
									aliased_unification,alias_dict = generate_alias(newfeat,newfeat,prod.rhs(),frag_unif)
									####print('\n\n...generate 1')
									filled_copied_unification = repopulate_alias(newfeat,alias_dict,aliased_unification)	
									####print(aliased_unification)
									####print(alias_dict)
									####print(filled_copied_unification)

									########print(filled_copied_unification)
									########print('++===')
							
									#now we need to propagate back to the LHS, but only keeping the values we originally
									#had on the LHS
									new_new_unif = strip_of_non_existant_elems_in_a(prod._lhs, filled_copied_unification)
										
									#Now add to the cache
									yield_item = (frag,new_new_unif)
									if(item in CACHE):
										if(not yield_item in CACHE[item]):
											CACHE[item].append(yield_item)
									else:
										CACHE[item] = []
										CACHE[item].append(yield_item)
										
									yield(yield_item)
									
								else:	
									#it was a terminal... propagate accordingly
									#print_with_depth('Unifies:',print_depth,3)
									#print_with_depth(str(frag),print_depth,3)
									#print_with_depth(fstr_to_single_line_str(newfeat,print_depth),print_depth,3)
									
									new_new_unif = strip_of_non_existant_elems_in_a(prod._lhs, newfeat)
									yield(frag, new_new_unif)
		else:
			#If this element is a terminal (aka string) then we're done!
			# return : item itself as a list and its non-existent feat-struct
			#print_with_depth('terminal: ' + str(([item],None)),print_depth,3)
			yield ([item],None)

def _generate_all(grammar,items,depth, print_depth=0):
	""" Expand multiple items (aka a rhs)""" 
	
	if items:
		#expand the items
		#print_with_depth(to_single_line_str(items),print_depth,3)
		try: 
		
			#get all the expansions for the first of the items, then add to that all the expansions of the other items...
			for (frag1,frag1_unif) in _generate_one(grammar, items[0], depth,print_depth+1):
				
				#now expand all the subsequent items for this version of item[0]
				if(items[1:]):
					for (frag2, frag2_unif) in _generate_all(grammar, items[1:], depth, print_depth+1):
					
						#if either of these are none, they were terminal - so they unify
						if(frag1_unif == None):
							did_unify = True
							unification = frag2_unif
						elif(frag2_unif == None):
							did_unify = True
							unification = frag1_unif
							
						else:
							#do the read unification...
							frag1_unif = clean_unification_of_type_and_id(frag1_unif)
							frag2_unif = clean_unification_of_type_and_id(frag2_unif)
							
							####print(type(frag1_unif))
							####print(type(items[0]))
							####print(isinstance(frag1_unif,FeatStruct))
							####print(isinstance(items[0],FeatStruct))
							
							#create an alias, since variables can be associated with different features
							aliased_unification, alias_dict = generate_alias(items[0],frag1_unif,items[1:],frag2_unif)
		
							####print('=========')
							#we do a regular unification here because we actually want to propagate all the features 
							#since we don't know which ones the LHS will want
							#BUT! we use aliased-unification as the proof of unification
							unification = unify(frag1_unif,frag2_unif,rename_vars = False) #false
							####print('---')
							####print(unification)

							if(aliased_unification):
								did_unify = True
							else:
								did_unify = False
								
						
						if(did_unify):
							#print_with_depth('Unified: ' + str((frag1,frag2)),print_depth,3)
						
							#print_with_depth(fstr_to_single_line_str(unification,print_depth),print_depth,3)
							yield (frag1 + frag2, unification)
						#else:
							#print_with_depth('Unify Failed:',print_depth,3)
							#print_with_depth(str((frag1,frag2)),print_depth,3)
							#print_with_depth(fstr_to_single_line_str(frag1_unif,print_depth),print_depth,3)
							#print_with_depth(fstr_to_single_line_str(frag2_unif,print_depth),print_depth,3)

							
				#if there aren't any more items (ie rhs was only item[0])
				else:
						yield (frag1, frag1_unif)
		except RuntimeError as _error:
			if _error.message == "maximum recursion depth exceeded":
                # Helpful error message while still showing the recursion stack.
				raise RuntimeError("The grammar has rule(s) that yield infinite recursion!!")		
	else: 
		#...if there are no items
		print_with_depth('...generate all terminal:' + str(([],None)),print_depth,3)
		#input()
		yield ([], None)			
			
def main():
	global START
	#this is your start symbol - set to COMMAND for the real deal
	START = 'COMMAND' 
	
	#just write the summary to an output file (if you want all the print statements, pipe it via command line)
	f = 'debug_summary.out'
	fout = open(f,'w')
	fout.write('summary:\n')
	
	config_dict = {'NOUN_SYNONYM': False, 'NOT_DEBUG': False, 'VERB_SYNONYMS':False, 'VERB_PERSON_SYNONYMS':False}
	generator,grammar = read_fcfgs_and_create_generator(config_dict)
	print('done getting generator')
	#sentences = set()
	sentences = {}
	for sentence in generator:

		#the words
		s = sentence[0]
		#the feat struct
		p = sentence[1]

		#convert to a string (instead of a list of strings)
		t = ' '.join(s)
		t = re.sub(' +',' ',t)
		
		#this produces the slots
		ss = convert_unif_to_slot_dict(p)
		
		#print(t)
		#print(p)
		#print('---')
		#print(slts)
		#sentences.add(t)
		sentences[t] = (p,ss)
		source.write(t + '\n')
		#fout.write(str(p) + '\n')
		string=""

		#order priority: intent, action, device, all others
		if 'intent' in ss.keys():
			string+='intent'+"["+str(ss['intent'])+"], "
			del ss['intent']
		if 'action' in ss.keys():
			string+='action'+"["+str(ss['action'])+"], "
			del ss['action']
		if 'device' in ss.keys():
			string+='device'+"["+str(ss['device'])+"], "
			del ss['device']
		for label,value in ss.items():	
			string+=label+"["+str(value)+"], "
		string=string.strip(', ')
		target.write(string)
		target.write('\n')	
		#fout.write('Slots: '+pprint.pformat(ss) + '\n')
		#fout.write('---\n')
		
	
	print('summary:') #sorted(sentences)
	#fout.write('summary:\n')
	
	#for sentence in sentences:
		#print(sentence)
		#fout.write(sentence + '\n')
		#print(sentences[sentence][0])
		#print('---')
		#print(pprint.pformat(sentences[sentence][1],width=40))
		#fout.write(str(sentences[sentence]) + '\n')
		#print('---------------------------------------')
		#fout.write('---\n')
		
	#print('\n senetence summary:') #sorted(sentences)
	fout.write('\n senetence summary:\n')
	for sentence in sorted(sentences):
		print(sentence)	
		fout.write(sentence + '\n')
		
	fout.close()

def convert_unif_to_slot_dict(fs):
	#Convert the final fs to a dictionary of slots: 
	#Replace *type* with intent 
	#Flatten the fs (using - to join the names)
	#Remove elements with value 'none'
	output_dict = {}
	for feat in fs:
		#this is handy - ignore this feature name when 
		#composing the final slot name from a 
		#featstruct with depth
		curr_name = str(feat).lower()
		if(curr_name[0] == '~'):
			curr_name = ''
		
		#if its got depth, repeat and merge the names
		if(isinstance(fs[feat],FeatStruct)):
			retd = convert_unif_to_slot_dict(fs[feat])
			for subfeat in retd:
				if(curr_name != ''):
					name = curr_name+'-'+subfeat 
				else:
					name = subfeat
				value = retd[subfeat]
				add_thename(name,value,output_dict)
		
		#this is a special check - you might want to comment out this assert
		elif(isinstance(fs[feat],ExclusiveTrueFeatureValue)):
			assert(len(list(fs[feat].feat_list)) ==1)
			
			name = curr_name
			#incase you commented out the assert
			if(len(fs[feat].feat_list) == 0):
				value = {}
			else:
				value = list(fs[feat].feat_list)[0]
				
			add_thename(name,value,output_dict)
				
		#if the feature was never defined, just ignore it
		elif(isinstance(fs[feat], Variable)):
			name = 'none'
			value = 'none'
			add_thename(name,value,output_dict)
		else:
			name = curr_name
			value = fs[feat]
		
			add_thename(name,value,output_dict)
		
			
	return output_dict
	
def add_thename(name,value,d):
	#replace the _ with -
	name = re.sub('_','-',name)
		
	#ignore all none features and symbol names
	if(value != 'none' and name != '*type*'):
		d[name] = value 
	
	
def read_fcfgs_and_create_generator(config_dict = {}):
	""" Read and augment the basic grammar, using compile switches specified in config_dict
	"""
	#read the content
	content = read_multiple_files(FILES, START,config_dict)
	
	#turn it into a grammar
	grammar = nltk.grammar.FeatureGrammar.fromstring(content)
	
	#add the new productions
	new_prods = fake_the_device_groups(grammar)
	cont_add = '\n'.join(new_prods)
	content += '\n' + cont_add

	#write the new complete set of productions to file
	write_to_file('temp_content.fcfg',content)	

	#turn it into a grammar again
	grammar = nltk.grammar.FeatureGrammar.fromstring(content)
	
	#get the generator
	generator = get_generator(grammar)

	return generator, grammar
	
if __name__ == '__main__':

	main()

