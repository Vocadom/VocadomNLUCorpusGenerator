import json
import re



FILETESTHADORTH = open('test.seq.in','w')
FILETESTHADSLOT = open('test.seq.out','w')
FILETESTHADLAB = open('test.label','w')
FILETESTHADVAL = open('testval.seq.out','w')

#ALIGNMENT ORTHO TEXT - CHAR POS			

def write_sent(infile,sent):
		infile.write("%s\n" % (sent))

#SPLIT SENT
def splitsent(t):
			words = t.split('_')
			return words

def splitsent2(t):
			words2 = t.split(' ')
			return words2

def add_newline(infile):
	infile.write("\n")			

def write_slot_O(infilehadslot,string):
	infilehadslot.write("O ")



def write_slot_had(infilehadslot,slotstringdict,textline):

	dictlist=[]
	a = splitsent(textline)

	for word in a:
		pattern1=""
		b = splitsent2(word)
		wordb=str(word)

		pattern1=wordb
		
		
			

			
			

		for i in range(len(b)):
			


			if(i==0):

				if pattern1 in slotstringdict:
					infilehadslot.write("B-%s " % (slotstringdict[pattern1]))
				else:
					infilehadslot.write("O ")
			elif(i>0):

				if pattern1 in slotstringdict:
					infilehadslot.write("I-%s " % (slotstringdict[pattern1]))				
				else:
					infilehadslot.write("O ")
		
						






with open('test.json') as json_file:
	data=json.load(json_file)

	
	productions=data["rasa_nlu_data"]["common_examples"]

	for item in productions:	
		slotstringdict={}
		valstringdict={}
		slotswords = []
		substdict={}

		item['intent'] = re.sub('none','NONE',item['intent'])
		write_sent(FILETESTHADLAB,item['intent'])

		write_sent(FILETESTHADORTH,item['text'])
		textline = str(item['text'])

		origsentwords = splitsent2(textline)

		for z in item['entities']:							

		 	string=str(z['text'])
		 	string2 = "_" + string + "_"
		 	slot=str(z['entity'])
		 	value=str(z['value'])
		 	textline = re.sub(string,string2,textline)
		 	textline = re.sub('[ ]*_[ ]*','_',textline)
		 	textline = re.sub('[_]+','_',textline)
		 	textline = re.sub('_$','',textline)

		
			
				
				

		 	slotstringdict[string]=slot
		 	valstringdict[string]=value	

		write_slot_had(FILETESTHADSLOT,slotstringdict,textline)
		write_slot_had(FILETESTHADVAL,valstringdict,textline)
		add_newline(FILETESTHADSLOT)
		add_newline(FILETESTHADVAL)

			
