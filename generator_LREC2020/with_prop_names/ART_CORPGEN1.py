import nltk
import re
import codecs
from util import *
import sys
import re
from util_nltk_modifications import *
from nltk.grammar import Nonterminal
import nltk.featstruct
from nltk.sem.logic import Variable
import pprint
import random
from collections import OrderedDict

#from nltk.parse.generate import generate
START = 'COMMAND'
#these are the files you want to read
FILES = ['amiqual_commands.fcfg',
		'amiqual_specification.fcfg',
		'generic_smarthome_specification.fcfg',
		'vocab.fcfg']

#this determines which print statements actually get printed
DEBUG=3
	
	
def fake_the_device_groups(grammar):
	""" Produce new productions (ie S-> VP NP rules) for SpecDeviceGroup, which can be inferred from the 
		SpecDevice productions already in the grammar.
		(This allows you to not have to write them yourself.)
		It actually produces two new rules:
		
		1.
		SpecDeviceXGroup[AGR=[PLUR=plur,VOWEL=?v,GENDER=?g],Location=Y] -> 
			WordDeviceX[AGR=?a]
		
		where X is the device type (ex. Light) and Y is the location (where multiple of these devices are found)
		
		2. DeviceX[Z] -> SpecDeviceXGroup[AGR=?a,Location=?l]
	
		where Z is the same as for the DeviceX -> SpecDeviceX rules that this was derived from 
		
		: param grammar : the existing grammar
		: returns : a list of productions as strings
	"""
	prods = get_all_prod_of_type_starts_with(grammar, 'SpecDevice')
	new_prods = []
	
	#separate prods into their respective device tyeps
	device_types = sort_prod_based_on_lhs_feature(prods, Feature('type'))	
	for devtype in device_types:
		locs = {}
		locs = sort_prod_based_on_lhs_feature(device_types[devtype],'Location')		
		newtype = devtype + 'Group'
		
		for loc in locs:
			if(len(locs[loc]) < 2):
				continue

			#pick one of the productions that has this location
			sample_prod = locs[loc][0]

			d = {'Location':{k:v for k, v in sample_prod._lhs['Location'].items()},
				'AGR':'[PLUR=plur,VOWEL=?v,GENDER=?g]'}

			str = new_prod_as_str({newtype:d},
									sample_prod._rhs.__str__()[1:-2])
									
			new_prods.append(str)
			
			#now also add the new device rule
			base_dev_type = devtype[4:] #get rid of the Spec
			sub_prods = get_all_prod_of_type_starts_with(grammar, base_dev_type)

			for sub_prod in sub_prods:

				new_lhs= {sub_prod._lhs[Feature('type')]:clean_featstruct_of_x(sub_prod._lhs,Feature('type'),True)}
				new_rhs = {newtype:clean_featstruct_of_x(sub_prod._rhs[0],Feature('type'),True)}
				str = new_prod_as_str(new_lhs,new_rhs)
				new_prods.append(str)
	

	return new_prods

CACHE = {}
def add_to_cache(item, cache_val):
	global CACHE
	if(item in CACHE):
		if(not cache_val in CACHE[item]):
			CACHE[item].append(cache_val)
	else:
		CACHE[item] = []
		CACHE[item].append(cache_val)		
	
def get_generator(grammar):
	""" Generate all possible productions given a grammar. 
		THIS IS YOUR MAIN GENERATION FUNCTION
		: returns : the generator that will yield all sentences in the grammar
	"""
	#get the start symbol
	start = grammar.start()
	
	#run to maximum system depth
	depth = sys.maxsize
	
	#generate all productions for the start symbol
	iter = _generate_all(grammar, [start], depth)
	#return the generator
	return iter		
		
def _generate_one(grammar, item, depth,print_depth= 0):
	""" Fully generate a single element in the grammar
		ie given a RHS symbol, possibly with a partially initialized feature struct, 
		get all possible fully specified replacements for this symbol
		: yields: (all of) a word expansion of item, and a unification for that item
	"""
	
	if depth > 0:

		if isinstance(item, Nonterminal):
			#If we've done this already, just use what we put in the cache.
			if(item in CACHE):
				#print_with_depth('Using cache...', print_depth,3)

				for elem in CACHE[item]:
					yield(elem)
			else:
				#If it's not terminal... get all the possible expansions...
			
				#...only consider the productions starting with this item
				for prod in grammar.productions(lhs=item):
					#print_with_depth(str(prod),print_depth,3)
					#...only match productions which meet the criteria (feat-struct must match)
					
					#rename the variables since they come from different productions
					#unifying LHS of the new production with our original symbol
					#if for example, the rule is only for PLUR=plur and our symbol already has PLUR=sing then 
					#this rule isn't useful to us
					unification = unify(item,prod._lhs,rename_vars = True)
					if(unification):

						#now we need to replace this single itme with its expanded production rule
						for (frag,frag_unif) in _generate_all(grammar, prod.rhs(), depth-1,print_depth+1):
							copied_unification = clean_unification_of_type_and_id(unification)
							
							#now unify the new expansion with the current lhs info (if possible)
							if(frag_unif != None):
								#just do a basic unification for now, to make sure all the filled elements are correct
								frag_unif = clean_unification_of_type_and_id(frag_unif)
								new_unif = unify(copied_unification,frag_unif,rename_vars = 
	 True)
								unifies = new_unif != None
							
							else:
								#no featstruct form the rhs meant it was a terminal and thus definitely unifies
								new_unif = copied_unification
								unifies = True
							
							if(unifies):
								
								newfeat = replace_featstructa_elems_with_elems_from_b(unification,new_unif)
								
								if(frag_unif != None):

									
									#now do the aliased unification in order to propagate the values correctly to the LHS
									aliased_unification,alias_dict = generate_alias(newfeat,newfeat,prod.rhs(),frag_unif)

									filled_copied_unification = repopulate_alias(newfeat,alias_dict,aliased_unification)	



							
									#now we need to propagate back to the LHS, but only keeping the values we originally
									#had on the LHS
									new_new_unif = strip_of_non_existant_elems_in_a(prod._lhs, filled_copied_unification)
										
									#Now add to the cache
									yield_item = (frag,new_new_unif)
									if(item in CACHE):
										if(not yield_item in CACHE[item]):
											CACHE[item].append(yield_item)
									else:
										CACHE[item] = []
										CACHE[item].append(yield_item)
										
									yield(yield_item)
									
								else:	
									#it was a terminal... propagate accordingly

									
									new_new_unif = strip_of_non_existant_elems_in_a(prod._lhs, newfeat)
									yield(frag, new_new_unif)
		else:
			#If this element is a terminal (aka string) then we're done!
			# return : item itself as a list and its non-existent feat-struct
			#print_with_depth('terminal: ' + str(([item],None)),print_depth,3)
			yield ([item],None)

def _generate_all(grammar,items,depth, print_depth=0):
	""" Expand multiple items (aka a rhs)""" 
	
	if items:
		#expand the items
		#print_with_depth(to_single_line_str(items),print_depth,3)
		try: 
		
			#get all the expansions for the first of the items, then add to that all the expansions of the other items...
			for (frag1,frag1_unif) in _generate_one(grammar, items[0], depth,print_depth+1):

				#now expand all the subsequent items for this version of item[0]
				if(items[1:]):
					#frag1.append('x')                    
					for (frag2, frag2_unif) in _generate_all(grammar, items[1:], depth, print_depth+1):
					
						#if either of these are none, they were terminal - so they unify
						if(frag1_unif == None):
							did_unify = True
							unification = frag2_unif
						elif(frag2_unif == None):
							did_unify = True
							unification = frag1_unif
							
						else:
							#do the read unification...
							frag1_unif = clean_unification_of_type_and_id(frag1_unif)
							frag2_unif = clean_unification_of_type_and_id(frag2_unif)
							

							
							#create an alias, since variables can be associated with different features
							aliased_unification, alias_dict = generate_alias(items[0],frag1_unif,items[1:],frag2_unif)
		
							####print('=========')
							#we do a regular unification here because we actually want to propagate all the features 
							#since we don't know which ones the LHS will want
							#BUT! we use aliased-unification as the proof of unification
							unification = unify(frag1_unif,frag2_unif,rename_vars = False) #false


							if(aliased_unification):
								did_unify = True
							else:
								did_unify = False
								
						
						if(did_unify):

							frag1.append('xxxxx')
							#print_with_depth('Unified: ' + str((frag1,frag2)),print_depth,3)
						
							#print_with_depth(fstr_to_single_line_str(unification,print_depth),print_depth,3)
							yield (frag1 + frag2, unification)
							#print(frag2)
						#else:
							#print_with_depth('Unify Failed:',print_depth,3)
							#print_with_depth(str((frag1,frag2)),print_depth,3)
							#print_with_depth(fstr_to_single_line_str(frag1_unif,print_depth),print_depth,3)
							#print_with_depth(fstr_to_single_line_str(frag2_unif,print_depth),print_depth,3)

							
				#if there aren't any more items (ie rhs was only item[0])
				else:
						yield (frag1, frag1_unif)
		except RuntimeError as _error:
			if _error.message == "maximum recursion depth exceeded":
                # Helpful error message while still showing the recursion stack.
				raise RuntimeError("The grammar has rule(s) that yield infinite recursion!!")		
	else: 
		#...if there are no items
		#print_with_depth('...generate all terminal:' + str(([],None)),print_depth,3)
		#input()
		yield ([], None)

#CORRECTIONS

def corrections(t):
		t = re.sub(' dans [xxxxx ]+',' dans ',t)
		t = re.sub(' sur [xxxxx ]+',' dans ',t)		
		t = re.sub(' mon [xxxxx ]+',' mon ',t)
		t = re.sub(' ma [xxxxx ]+',' ma ',t)	
		t = re.sub(' mes [xxxxx ]+',' mes ',t)
		t = re.sub(' son [xxxxx ]+',' son ',t)
		t = re.sub(' sa [xxxxx ]+',' sa ',t)	
		t = re.sub(' ses [xxxxx ]+',' ses ',t)		
		t = re.sub(' le [xxxxx ]+',' le ',t)
		t = re.sub(' la [xxxxx ]+',' la ',t)
		t = re.sub(' les [xxxxx ]+',' les ',t)
		t = re.sub(' des [xxxxx ]+',' des ',t)
		t = re.sub(' en [xxxxx ]+',' en ',t)		
		t = re.sub(' ce [xxxxx ]+',' ce ',t)
		t = re.sub(' cette [xxxxx ]+',' cette ',t)
		t = re.sub(' ces [xxxxx ]+',' ces ',t)
		t = re.sub(' quel [xxxxx ]+',' quel ',t)
		t = re.sub(' quelle [xxxxx ]+',' quelle ',t)
		t = re.sub(' quels [xxxxx ]+',' quels ',t)
		t = re.sub(' quelles [xxxxx ]+',' quelles ',t)									
		t = re.sub(' de [xxxxx ]+',' de ',t)
		t = re.sub(' du [xxxxx ]+',' du ',t)
		t = re.sub(' d\' [xxxxx ]+',' d\' ',t)
		t = re.sub(' l\' [xxxxx ]+',' l\' ',t)																
		t = re.sub('(xxxxx )+','_',t)
		t = re.sub('xxxxx','_',t)
		t = re.sub('_ +','_',t)
		t = re.sub(' +_','_',t)
		t = re.sub(' +_','_',t)	
		t = t + "_"
		t = re.sub('[_]+','_',t)
		t = re.sub('d\' ','d\'',t)
		t = re.sub('l\' ','l\'',t)	
		t = re.sub('_à_','_à ',t)
		t = re.sub('_par_','_par ',t)
		t = re.sub('_celsius',' celsius',t)
			
		return t				

#align non slot ortho words to dictionary slots
def corr_dict(sent,dict):

		
	patterns = ['vocadom','ichefix','ichéfix','ichefixe','ichéfixe','berenio','bérénio','minouche','teraphim','téraphim','cirrus','cirus','hestia','ulysse','messire','peux','plaît','est-ce que','faut','besoin','est-il','est-elle','sont-ils','sont-elles']
	#patterns = ['KEYWORD','peux','plaît','est-ce que','faut','besoin','est-il','est-elle','sont-ils','sont-elles']
	patterns2 = ['david','supermarché','médecin','fille','pompiers','toubib','infirmière','ambulance','samu','docteur','température','volume']
	patterns3 = ['cuisine','maison','salon']
	patterns4 = ['du rez-de-chaussée']
	#patterns5 = ['fermé','ouvert']
	#move dictionary slots/labels to tupple list
	dictlist=[]
	for key, value in dict.items():
		temp = (key,value)
		dictlist.append(temp)    
				
	#split ortho sentence on '_' into slots
	entities = splitsent(sent)
	print(sent)
		
	for x in dictlist:
		if re.search("device-setting",str(x[0])):
			dictlist.remove(x)
			dictlist.append(x)	




	for e in range(len(entities)):
		for p in patterns:
			if re.search(str(p),str(entities[e])):
				entitystr="O" + "_" + str(e)
				valuestr="O" + "_" + str(e)
				dictlist.insert(e,(entitystr,valuestr))	
	#correct dict positions	
			
	for f in range(len(entities)):
		#David bowie
		if re.search(str(patterns2[0]),str(entities[f])):
			entitystr="value-artist"
			valuestr="artistname"
			dictlist.insert(f,(entitystr,valuestr))	
		#supermarché
		if re.search(str(patterns2[1]),str(entities[f])):
			entitystr="organization"
			valuestr="supermarket"
			dictlist.insert(f,(entitystr,valuestr))			
		#medecin
		if re.search(str(patterns2[2]),str(entities[f])):
			entitystr="person-occupation"
			valuestr="doctor"
			dictlist.insert(f,(entitystr,valuestr))
		#fille
		if re.search(str(patterns2[3]),str(entities[f])):
			entitystr="person-relation"
			valuestr="daughter"
			dictlist.insert(f,(entitystr,valuestr))
		#pompiers
		if re.search(str(patterns2[4]),str(entities[f])):
			entitystr="organization"
			valuestr="fire_brigade"
			dictlist.insert(f,(entitystr,valuestr))
		#toubib
		if re.search(str(patterns2[5]),str(entities[f])):
			entitystr="person-occupation"
			valuestr="doctor"
			dictlist.insert(f,(entitystr,valuestr))
		#infirmière
		if re.search(str(patterns2[6]),str(entities[f])):
			entitystr="person-occupation"
			valuestr="doctor"
			dictlist.insert(f,(entitystr,valuestr))
		#ambulance
		if re.search(str(patterns2[7]),str(entities[f])):
			entitystr="organization"
			valuestr="emergency"
			dictlist.insert(f,(entitystr,valuestr))
		#samu
		if re.search(str(patterns2[8]),str(entities[f])):
			entitystr="organization"
			valuestr="samu"
			dictlist.insert(f,(entitystr,valuestr))
		#docteur
		if re.search(str(patterns2[9]),str(entities[f])):
			entitystr="person-occupation"
			valuestr="doctor"
			dictlist.insert(f,(entitystr,valuestr))

		#température
		if re.search(str(patterns2[10]),str(entities[f])):
			entitystr="room-property"
			valuestr="temperature"
			dictlist.insert(f,(entitystr,valuestr))
		#volume
		if re.search(str(patterns2[11]),str(entities[f])):
			entitystr="device-component"
			valuestr="volume"
			dictlist.insert(f,(entitystr,valuestr))	

		

	
	newdict={}
	for a,b in dictlist:
		newdict[a]=b

	return newdict
		
#SPLIT SENT
def splitsent(t):
			words = t.split('_')
			return words

def splitsent2(t):
			words2 = t.split(' ')
			return words2			

def add_newline(infile):
	infile.write("\n")
				
#ALIGNMENT ORTHO TEXT - CHAR POS			
def findpos(string,t):
	begin = str(t).find(string)
	fin = begin + len(string)
	return(begin,fin)

#Opattern: no empty slots for Json format

def Opattern(input):

	if re.search("^O_[\d]+",str(input)):
		return input
		


#JSON WRITE FORMAT 
def write_head(infile):
		infile.write("{\n  \"rasa_nlu_data\": {\n    \"common_examples\": [")
def write_sent_head(infile,sent):
		infile.write("\n      {\n        \"text\": \"%s\",\n        \"entities\": [\n" % (sent))
def write_sent_mid(infile,begin,fin,cle,val,strin):	
					infile.write("          {\n            \"start\": %d,\n\
            \"end\": %d,\n            \"entity\": \"%s\",\n\
            \"value\": \"%s\",\n            \"text\": \"%s\"\n          },\n" % (begin,fin,cle,val,strin))
def write_sent_fin(infile,begin,fin,cle,val,strin):	
					infile.write("          {\n            \"start\": %d,\n\
            \"end\": %d,\n            \"entity\": \"%s\",\n\
            \"value\": \"%s\",\n            \"text\": \"%s\"\n          }\n" % (begin,fin,cle,val,strin))   
def write_intent(infile,string,count):
		infile.write("        ],\n        \"intent\": \"%s\",\n        \"ordre_id\": \"%d\"\n      }," % (string,count))
def	write_tail(infile):
	infile.write("\n    ]\n  }\n}")
	
#ATT-RNN HADOOP WRITE FORMAT
#write file with ortho input(train.seq.in/test.seq.in)
def write_sent_had(infilehadorth,sent):
	infilehadorth.write("%s\n" % (sent))
	
#write file with intent labels(train.label/test.label)	
def write_label_had(infilehadlab,label):
	infilehadlab.write("%s\n" % (label))	

#write file with slotentity labels(train.seq.out/test.seq.out)
#write file with slotval labels(trainval.seq.out/testval.seq.out)	
def write_slot_had(infilehadslot,slot,string):
	

	a = splitsent2(string)
	for i in range(len(a)):
		
		if(i==0):

			slot = re.sub('^O_[\d]+','O',str(slot))
			slot = re.sub('^B-O$','O',str(slot))

			if (not(re.search('^O$',slot))):

				infilehadslot.write("B-%s " % (slot))
			else:
				infilehadslot.write("%s " % (slot))
		else:

			slot = re.sub('^O_[\d]+','O',str(slot))
			slot = re.sub('^I-O$','O',str(slot))

			if (not(re.search('^O$',slot))):
			
				infilehadslot.write("I-%s " % (slot))
			else:
				infilehadslot.write("%s " % (slot))



def write_intentsent_tricrf(infiletri,infiletri2,orth,slotlab):
	keylist = []
	stringlist = []
	tuples = []
	tuples2 = []
	keycounter=-1
	orthosent = re.sub('[_]+',' ',orth)
	listintent = []
	
	a=splitsent(orth)
	splitted=a
	for key,value in slotlab.items():
		
		string=""
		pattern1 = 'intent'		
	
		if re.search(pattern1,  str(key)):		
		 	listintent.append(value)
		else:
			keycounter+=1			
			string = (a[keycounter])
			splitstring = splitsent2(string)
			for i in range(len(splitstring)):
				if(i==0):
					stringkey = str(key) + "-B"
					stringval = str(value) + "-B" 
					tuples.append((stringkey,splitstring[i]))
					tuples2.append((stringval,splitstring[i]))
				else:
					stringkey2 = str(key) + "-I"
					stringval2 = str(value) + "-I"
					tuples.append((stringkey2,splitstring[i]))	
					tuples2.append((stringval2,splitstring[i]))	

	infiletri.write("%s %s\n" % (listintent[0],orthosent))
	infiletri2.write("%s %s\n" % (listintent[0],orthosent))
	del listintent[:]

	tuple_proc(tuples,infiletri,splitted)
	tuple_proc(tuples2,infiletri2,splitted)

def tuple_proc(tupl,filein,splitfile):
	abcounter=0
	i = 1		
	for a,b in tupl:
		dupa = re.sub('^O_[\d]+-B','NONE',a)
		a = re.sub('^O_[\d]+-I','NONE',dupa)
		
		abcounter+=1
		if(abcounter == 1):
			filein.write("%s word=%s word-1=<s> " % (a,b))
		
		elif(abcounter == 2):
			filein.write("%s word=%s word-2=<s> word-1=%s " % (a,b,splitfile[0]))
			
		else:
			filein.write("%s word=%s " % (a,b))
						
		j = -3
		for x in (tupl[i-3: i-1]):
			j += 1
			filein.write("word%d=%s " % (j,x[1]))
		if(i==len(tupl)):
			filein.write("word+1=</s>")
			
		k = 0
		for y in (tupl[i: i+2]):
			
			k += 1
			if(i<(len(tupl)-1)):			
				filein.write("word+%d=%s " % (k,y[1]))
			elif(i==(len(tupl)-1)):
				filein.write("word+%d=%s word+2=</s>" % (k,y[1]))
									
		filein.write("\n")	
		i += 1	
	filein.write("\n")

	


#JSON/TRI-CRF/ATT-RNN HADOOP FORMAT GENERATOR
def json_format(generator):
	counter=0
	#JSON RASA (lin CRF/SVM) FILES
	FILETRAIN = open('DOMOTIC_TRAIN_rasa.json','w')
	FILETEST = open('DOMOTIC_TEST_rasa.json','w')
	FILETEST2 = open('DOMOTIC_TEST2_rasa.json','w')
	#HADOOP FILES
	FILETRAINHADORTH = open('train.seq.in','w')
	FILETESTHADORTH = open('test.seq.in','w')
	FILETEST2HADORTH = open('test2.seq.in','w')
	FILETRAINHADSLOT = open('train.seq.out','w')
	FILETESTHADSLOT = open('test.seq.out','w')
	FILETEST2HADSLOT = open('test2.seq.out','w')
	FILETRAINHADLAB = open('train.label','w')
	FILETESTHADLAB = open('test.label','w')
	FILETEST2HADLAB = open('test2.label','w')
	FILETRAINHADVAL = open('trainval.seq.out','w')
	FILETESTHADVAL = open('testval.seq.out','w')
	FILETEST2HADVAL = open('test2val.seq.out','w')	
	#TRI-CRF FILES
	FILETRAINLABTRI = open('minwoo_BIO.labels.train.data','w')
	FILETESTLABTRI = open('minwoo_BIO.labels.test.data','w')
	FILETEST2LABTRI = open('minwoo_BIO.labels.test2.data','w')
	FILETRAINVALTRI = open('minwoo_BIO.values.train.data','w')
	FILETESTVALTRI = open('minwoo_BIO.values.test.data','w')	
	FILETEST2VALTRI = open('minwoo_BIO.values.test2.data','w')		
	write_head(FILETRAIN)  
	write_head(FILETEST)
	write_head(FILETEST2)

	listintent = []
	sentences = {}
	listsent = []
	mylist = {}
	#for sentence in generator:
		
	
	for sentence in generator:
			
		counter+=1
		#partition = 40
		partition = 10
		
		

		keycounter=-1
		
		
		print("sentence %d\n" % (counter))


		
		s=sentence[0]
		p=sentence[1]
		
		ss = convert_unif_to_slot_dict(p)		
		t = ' '.join(s)
		t = corrections(t)
		tcorr = t
		
		###dictionary corrections, insertion of words 'outside' slots: est-ce que tu peux, KEYWORD...
		moddict = corr_dict(t,ss)
		ss = moddict
		print(ss)
		################################################################################################
		a=splitsent(t)
		t = re.sub('[_]+',' ',t)
		
		
		
		if counter % partition == 0:
			#sentence for test file rasa format
			write_sent_head(FILETEST,t)
			#sentence for test file hadoop format
			write_sent_had(FILETESTHADORTH,t)
		
		elif counter % partition == 1:
			#sentence for test file rasa format
			write_sent_head(FILETEST2,t)
			#sentence for test file hadoop format
			write_sent_had(FILETEST2HADORTH,t)	

		else:
			#sent for train file rasa format
			write_sent_head(FILETRAIN,t)
			#sent for train file hadoop format
			write_sent_had(FILETRAINHADORTH,t)
										
		for key,value in ss.items():
			string=""
			pattern1 = 'intent'		
			if re.search(pattern1,  str(key)):		
		 		listintent.append(value)
		 		
			else:
				keycounter+=1			
				string = (a[keycounter])
				(start,end) = findpos(string,t)
				if(keycounter < (len(ss)-2)):
					if counter % partition == 0:
						if(not(Opattern(key))):
							#RASA
							write_sent_mid(FILETEST,start,end,key,value,string)
						#HADOOP
						write_slot_had(FILETESTHADSLOT,key,string)
						write_slot_had(FILETESTHADVAL,value,string)
					elif counter % partition == 1:
						if(not(Opattern(key))):
							#RASA
							write_sent_mid(FILETEST2,start,end,key,value,string)
						#HADOOP
						write_slot_had(FILETEST2HADSLOT,key,string)
						write_slot_had(FILETEST2HADVAL,value,string)	
					else:
					 	
					 	#HADOOP
					 	write_slot_had(FILETRAINHADSLOT,key,string)
					 	write_slot_had(FILETRAINHADVAL,value,string)
					 	
					 	if(not(Opattern(key))):
							#RASA
					 		write_sent_mid(FILETRAIN,start,end,key,value,string) 	
												
											 	    				
				else:
					if counter % partition == 0:

						#HADOOP
						write_slot_had(FILETESTHADSLOT,key,string)
						write_slot_had(FILETESTHADVAL,value,string)
						
						add_newline(FILETESTHADSLOT)
						add_newline(FILETESTHADVAL)
						
						#RASA
						if(not(Opattern(key))):
							write_sent_fin(FILETEST,start,end,key,value,string)
					elif counter % partition == 1:
						#HADOOP
						write_slot_had(FILETEST2HADSLOT,key,string)
						write_slot_had(FILETEST2HADVAL,value,string)
					
						add_newline(FILETEST2HADSLOT)
						add_newline(FILETEST2HADVAL)
						
						#RASA
						if(not(Opattern(key))):
							write_sent_fin(FILETEST2,start,end,key,value,string)						
						
					else:

						#HADOOP
						write_slot_had(FILETRAINHADSLOT,key,string)
						write_slot_had(FILETRAINHADVAL,value,string)
						
						add_newline(FILETRAINHADSLOT)
						add_newline(FILETRAINHADVAL)						

						#RASA
						if(not(Opattern(key))):
							write_sent_fin(FILETRAIN,start,end,key,value,string)

						
		if counter % partition == 0:
			#intent for rasa format									
			write_intent(FILETEST,str(listintent[0]),counter)
			#intent label for hadoop format
			write_label_had(FILETESTHADLAB,str(listintent[0]))
			write_intentsent_tricrf(FILETESTLABTRI,FILETESTVALTRI,tcorr,ss)

		elif counter % partition == 1:
			#intent for rasa format									
			write_intent(FILETEST2,str(listintent[0]),counter)
			#intent label for hadoop format
			write_label_had(FILETEST2HADLAB,str(listintent[0]))
			write_intentsent_tricrf(FILETEST2LABTRI,FILETEST2VALTRI,tcorr,ss)			
			
	
			
		else:
			#intent for rasa format									
			write_intent(FILETRAIN,str(listintent[0]),counter)
			#intent label for hadoop format
			write_label_had(FILETRAINHADLAB,str(listintent[0]))
			write_intentsent_tricrf(FILETRAINLABTRI,FILETRAINVALTRI,tcorr,ss)
										
		del listintent[:]
	write_tail(FILETEST)
	write_tail(FILETEST2)
	write_tail(FILETRAIN)		      	
    			
def main():



	global START
	#this is your start symbol - set to COMMAND for the real deal
	START = 'COMMAND' 
	
	#just write the summary to an output file (if you want all the print statements, pipe it via command line)

	#f = 'debug_summary.out'

	#fout = open(f,'w')

	#fout.write('summary:\n')
	
	config_dict = {'NOUN_SYNONYM': False, 'NOT_DEBUG': False, 'VERB_SYNONYMS':False, 'VERB_PERSON_SYNONYMS':False}

    
	generator,grammar = read_fcfgs_and_create_generator(config_dict)
	json_format(generator)

	print('done getting generator')
	#FILETRAIN.write(str(generator))
	#sentences = set()
	sentences = {}
	for sentence in generator: 
		#the words
		#NEXT FORMAT TO KEEP:
		s = sentence[0]
		#FILETRAIN.write(str(sentence[0]) + '\t')
		#FILETRAIN.write(str(s))
		#the feat struct
		#FILETRAIN.write(str(sentence[1]) + '\n')
		p = sentence[1]
		print(p)
		#convert to a string (instead of a list of strings)
		t = ' '.join(s)
		t = re.sub(' +',' ',t)
		
		#this produces the slots
		# ss = {'action': 'TURN_OFF', 'location-house': True, 'intent': 'set_device', 'device': 'music_player', 'location-room': 'salon', 'location-etage': 0}
		ss = convert_unif_to_slot_dict(p)
		#NEXT FORMAT TO KEEP:
		#FILETRAIN.write(str(ss) + '\n\n')		
		#print(t)
                #t = ortho sentences
		#
		#FILETRAIN.write(t)
		#FILETRAIN.write(str(p))
		print(p)
		print('---')
		#print(slts)
		#sentences.add(t)
		sentences[t] = (p,ss)
		#fout.write(t + '\n')
		#fout.write(str(p) + '\n')
		#FILETRAIN.write(str(t) + '\n')
		#fout.write(pprint.pformat(ss) + '\n')
		#FILETRAIN.write(pprint.pformat(ss) + '\n')
		#fout.write('---\n')
		
	
	#print('summary:') #sorted(sentences)
	#fout.write('summary:\n')
	
	for sentence in sentences:
		print(sentence)
		#fout.write(sentence + '\n')
		print(sentences[sentence][0])
		print('---')
		print(pprint.pformat(sentences[sentence][1],width=40))
		#fout.write(str(sentences[sentence]) + '\n')
		print('---------------------------------------')
		#fout.write('---\n')
		
	#print('\n senetence summary:') #sorted(sentences)
	#fout.write('\n senetence summary:\n')
	#for sentence in sorted(sentences):
	#	print(sentence)	
	#	fout.write(sentence + '\n')
	#	#FILETRAIN.write(sentence + '\n')
		
	#fout.close()

def convert_feat_list(fs):
	output_list = []
	for feat in fs:

		#print(feat,fs[feat])
		curr_name=str(feat).lower()
		#print(curr_name)
		#if(curr_name[0] == '~'):
		#	curr_name = ''
		output_list.append((curr_name,fs[feat]))
		

		#if its got depth, repeat and merge the names
		#if(isinstance(fs[feat],FeatStruct)):
			#retd = convert_feat_list(fs[feat])
			#print(retd)
			#for subfeat in retd:
				#print(subfeat)
				#if(curr_name != ''):
					#name = curr_name+'-'+subfeat
				#print(subfeat)
				#else:
				#	name = subfeat
				#value = retd[subfeat]
				#print(value)
				#output_list.append(name,value)
	for x,y in output_list:
		print(x,y)

				


def convert_unif_to_slot_dict(fs):
	#Convert the final fs to a dictionary of slots: 
	#Replace *type* with intent 
	#Flatten the fs (using - to join the names)
	#Remove elements with value 'none'
	
	output_dict = {}
	
	for feat in fs:
		
		
		#print("%s %s"%(feat,fs[feat]))
		curr_name=str(feat)
		#this is handy - ignore this feature name when 
		#composing the final slot name from a 
		#featstruct with depth
		curr_name = str(feat).lower()
		if(curr_name[0] == '~'):
			curr_name = ''
		
		#if its got depth, repeat and merge the names
		if(isinstance(fs[feat],FeatStruct)):
			retd = convert_unif_to_slot_dict(fs[feat])
			for subfeat in retd:
				if(curr_name != ''):
					name = curr_name+'-'+subfeat 
				else:
					name = subfeat
				value = retd[subfeat]
				add_thename(name,value,output_dict)
		
		#this is a special check - you might want to comment out this assert
		elif(isinstance(fs[feat],ExclusiveTrueFeatureValue)):
			assert(len(list(fs[feat].feat_list)) ==1)
			
			name = curr_name
			#incase you commented out the assert
			if(len(fs[feat].feat_list) == 0):
				value = {}
			else:
				value = list(fs[feat].feat_list)[0]
				
			add_thename(name,value,output_dict)
				
		#if the feature was never defined, just ignore it
		elif(isinstance(fs[feat], Variable)):
			name = 'none'
			value = 'none'
			add_thename(name,value,output_dict)
		else:
			name = curr_name
			value = fs[feat]
		
			add_thename(name,value,output_dict)
		
			
	return output_dict
	
def add_thename(name,value,d):
	#replace the _ with -
	name = re.sub('_','-',name)
		
	#ignore all none features and symbol names
	if(value != 'none' and name != '*type*'):
		d[name] = value 
	
	
def read_fcfgs_and_create_generator(config_dict = {}):
	""" Read and augment the basic grammar, using compile switches specified in config_dict
	"""
	#read the content
	content = read_multiple_files(FILES, START,config_dict)
	
	#turn it into a grammar
	grammar = nltk.grammar.FeatureGrammar.fromstring(content)
	
	#add the new productions
	new_prods = fake_the_device_groups(grammar)
	cont_add = '\n'.join(new_prods)
	content += '\n' + cont_add

	#write the new complete set of productions to file
	write_to_file('temp_content.fcfg',content)	

	#turn it into a grammar again
	grammar = nltk.grammar.FeatureGrammar.fromstring(content)


	#get the generator
	generator = get_generator(grammar)
	return generator, grammar
	
if __name__ == '__main__':

	main()

