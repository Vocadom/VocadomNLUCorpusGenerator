This is the repository of the corpus generator developped during the VOCADOM project founded by the French National Research Agency (Agence Nationale de la Recherche) / ANR-16-CE33-0006. If you use this tool or need more details please refer to the following paper: *Corpus generation for voice command in smart home and the effect of speech synthesis on End-to-End SLU*

```
@inproceedings{desot:hal-02861770,
  TITLE = {{Corpus generation for voice command in smart home and the effect of speech synthesis on End-to-End SLU}},
  AUTHOR = {Desot, Thierry and Portet, Fran{\c c}ois and Vacher, Michel},
  BOOKTITLE = {{12th Conference on Language Resources and Evaluation (LREC 2020)}},
  ADDRESS = {Marseille, France},
  SERIES = {Proceedings of the 12th Conference on Language Resources and Evaluation (LREC 2020), pages 6395--6404},
  PAGES = {6395--6404},
  YEAR = {2020}
}

```

Annotator contains the tool for automatic pre-annotating and an an interface for manual annotating natural corpora (VocADom, SWEET-HOME) in .trs format. The output is json of RASA format.

Generator_TSD2018 contains the grammar of artificial corpus (62k utterances). This version is a mix of English and French, so you will need to run a normalisation tool generator/normal.py on the output.

The readme file in folder Generator_TSD2018 explains explains its usage

Generator_LREC2020 contains the grammar of artificial corpus (77k utterances).
The readme file in folder Generator_LREC2020 explains its usage.

with_prop_names : is a version of the grammar that generates keyword proper namesm such as, 'vocadom'.\
with_keyword : is a version of the grammar that generates the keyword 'KEYWORD' for all generated utterances.




